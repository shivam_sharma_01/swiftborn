//
//  ViewController1.swift
//  pickerView
//
//  Created by FiveExceptions1 on 06/10/16.
//  Copyright © 2016 FiveExceptions1. All rights reserved.
//

import UIKit

class ViewController1: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate {
    var pickerView:UIPickerView!
    var list = ["1","2","3","4","5"]
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
        pickerView.delegate = self
        pickerView.dataSource = self
        self.view.addSubview(pickerView)
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 5
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return list[row]
    }
}
