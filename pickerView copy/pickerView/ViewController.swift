//
//  ViewController.swift
//  pickerView
//
//  Created by FiveExceptions1 on 06/10/16.
//  Copyright © 2016 FiveExceptions1. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var PickerView :UIPickerView!

//    var pickerView:UIPickerView?
    
   
    var pickerDataSource1 = ["White", "Red", "Green", "Blue"];
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        
//        let rect = CGRect(
//            x:self.view.frame.size.width * 0.1,
//            y:self.view.frame.size.height * 0.1,
//            width: self.view.frame.size.width * 0.8,
//            height: self.view.frame.size.height * 0.8)
//        pickerView = UIPickerView(frame:rect)
//        pickerView!.clipsToBounds = true
//        pickerView!.layer.borderWidth = 1
//        pickerView!.dataSource = self
//        pickerView!.delegate = self
//        self.view.addSubview(pickerView!)
    }
    
//    override func viewWillAppear(animated: Bool) {
//        PickerView.dataSource = self
//        PickerView.delegate = self
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

  
    @IBOutlet var numOfPeople: UITextField!
    
    @IBAction func getDpValue(sender: AnyObject) {
        print(numOfPeople.text,"<--value")
    }
    
    @IBAction func pickerGet(sender: UITextField) {
   
        
        print("UITapGestureRecognizer")
        //Create the view
        let inputView = UIView(frame: CGRectMake(0, 0, self.view.frame.width, 240))
//        self.view.addSubview(inputView)
        let inputButtonView = UIView(frame: CGRectMake(0, 0, self.view.frame.width, 40))
        inputButtonView.backgroundColor = UIColor.grayColor()
        inputView.addSubview(inputButtonView) // add UIView inside UIView
        
        //datepicker
        PickerView  = UIPickerView(frame: CGRectMake((self.view.frame.width/3) - 100, 40, 0, 0))
        PickerView.clipsToBounds = true
        PickerView.layer.borderWidth = 11
        PickerView.dataSource = self
        PickerView.delegate = self
        
        inputView.addSubview(PickerView) // add date picker to UIView
        
        print("----\(self.view.frame.size.width)")
        
        //        let image1 = UIImage(named: "foodicon.png")! as UIImage
        //        let image2 = UIImage(named: "halal.png")! as UIImage
        
        // done button-----------
        let doneButton = UIButton(frame: CGRectMake(10, 0, 50, 40))
        
        
        doneButton.setTitle("Done", forState: UIControlState.Normal)
        doneButton.setTitle("Done", forState: UIControlState.Highlighted)
        doneButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        doneButton.setTitleColor(UIColor.grayColor(), forState: UIControlState.Highlighted)
        doneButton.titleLabel?.font = UIFont.boldSystemFontOfSize(20)
        doneButton.tag = 12
        //        doneButton.setBackgroundImage(image1, forState: UIControlState.Normal)
        
        inputView.addSubview(doneButton) // add Button to UIView
        
        doneButton.addTarget(self, action: "doneButton:", forControlEvents: UIControlEvents.TouchUpInside) // set button click event
        
        let titleLable = UILabel(frame: CGRectMake( (self.view.frame.width/2) - ((100/2)-20), 0,60, 40 ) )
        titleLable.text = "Date"
        titleLable.textColor = UIColor.whiteColor()
        titleLable.font = UIFont.boldSystemFontOfSize(20.0)
        titleLable.tag = 13
        
        inputView.addSubview(titleLable) // add Label to UIView
        
        // cancel-----------
        let cancelButton = UIButton(frame: CGRectMake(self.view.frame.width - 80, 0,70, 40))
        cancelButton.setTitle("Cancel", forState: UIControlState.Normal)
        cancelButton.setTitle("Cancel", forState: UIControlState.Highlighted)
        cancelButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        cancelButton.setTitleColor(UIColor.grayColor(), forState: UIControlState.Highlighted)
        cancelButton.titleLabel?.font = UIFont.boldSystemFontOfSize(20)
        cancelButton.tag = 14
        //        cancelButton.setBackgroundImage(image2, forState: UIControlState.Normal)
        
        inputView.addSubview(cancelButton) // add Button to UIView
        
        cancelButton.addTarget(self, action: "cancelButton:", forControlEvents: UIControlEvents.TouchUpInside) // set button click event
        
        sender.inputView = inputView
     
        //  PickerView.addTarget(self, action: Selector("handleDatePicker:"), forControlEvents: UIControlEvents.ValueChanged)
        
        handleDatePicker(PickerView)
    }
    
    func handleDatePicker(sender: UIPickerView){
        print("handleDatePicker")
    }
    
    func numberOfComponentsInPickerView(PickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(PickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return pickerDataSource1.count
    }
    
    func pickerView(PickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        print(row)
        numOfPeople.text = pickerDataSource1[row]
        return pickerDataSource1[row]
    }
    
//    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
//        <#code#>
//    }
    
    func doneButton(sender:UIButton)
    {
        if sender.tag == 12
        {
            numOfPeople.resignFirstResponder() // To resign the inputView for num of people on clicking done.
        }
    }
    
    func cancelButton(sender:UIButton)
    {
        if sender.tag == 14
        {
            numOfPeople.resignFirstResponder() // To resign the inputView for num of people on clicking done.
        }
    }
    
   
}

