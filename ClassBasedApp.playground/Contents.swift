//: Playground - noun: a place where people can play

import UIKit



class abc {
    
    func abc_1(){
        print("hai abc")
    }
}

class def :abc {
    
    override func abc_1(){
        print("hai def")
    }
}

class User {
    var name: String!
    var age: Int!
    
    init(name: String, age: Int) {
        self.name = name
        self.age = age
    }
}

var name = User(name:"Rahul Khana--",age: 10)
print(name.name, name.age)