//: Playground - noun: a place where people can play



var str = "Hello, playground"
print(str)

let interestingNumbers = [
    
    "Prime":[2, 3, 5, 7, 11, 13],
    "Fiboncacci": [1, 1, 2, 3, 5, 8],
    "Square":[1, 4, 9, 16, 25],
]

for(kind, numbers) in interestingNumbers{
    
    print(kind, numbers)
    
}


var someDict:[Int:String] = [1:"One", 2:"Two", 3:"Three"]

var someVar = someDict[1]

print( "Value of key = 1 is \(someVar!)" )
print( "Value of key = 2 is \(someDict[2]!)" )
print( "Value of key = 3 is \(someDict[3]!)" )


func add(array:[Int]) -> Int{
    var aux = 0
    for i in array{
        aux += i
    }
    
    return aux
}

func addWithFunction(array:[Int], f:([Int])->Int ) -> (a:Int, b:Int){
    
    return ( f(array),array.count)
}

var array = [Int](0...4)
var result = addWithFunction(array: array, f: add)
print(result.a)
print(result.b)


func jediTrainer () -> ((String, Int) -> String) {
    func train(name: String, times: Int) -> (String) {
        return "\(name) has been trained in the Force \(times) times"
    }
    return train
}
let train = jediTrainer()

print(train("Obi Wan", 3))
