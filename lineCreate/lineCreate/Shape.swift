//
//  Shape.swift
//  lineCreate
//
//  Created by FiveExceptions1 on 18/10/16.
//  Copyright © 2016 FiveExceptions1. All rights reserved.
//

import UIKit

class Shape: UIView {

    @IBAction func makeLine(sender:UIButton)
    {
        let context = UIGraphicsGetCurrentContext()
            CGContextMoveToPoint(context!, 40, 40)
            CGContextAddLineToPoint(context!, 200, 40)
            CGContextSetLineWidth(context!, 5)
            CGContextSetFillColorWithColor(context!, UIColor.redColor().CGColor)
            CGContextStrokePath(context!)
    }

}
