//
//  ShapeView.swift
//  IOS9DrawShapesTutorial
//
//  Created by Arthur Knopper on 24/08/15.
//  Copyright © 2015 Arthur Knopper. All rights reserved.
//


import UIKit

class ShapeView: UIView {
    var currentShapeType: Int = 0
    var frameCustom :CGRect
    var degree:CGFloat = CGFloat()
   //    extension FloatingPoint {
//        var degreesToRadians: Self { return self * (22/7) / 180 }
//        var radiansToDegrees: Self { return self * 180 / (22/7) }
//    }
//
//    extension Int {
//        var degreesToRadians: Double { return Double(self) * (22/7) / 180 }
//        var radiansToDegrees: Double { return Double(self) * 180 / (22/7) }
//    }
    
    init(frame: CGRect, shape: Int) {
        print(frame.size.width)
        print(frame.size.height)
   
     
        
        
        frameCustom = frame
        super.init(frame: frame)
        self.currentShapeType = shape
        
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
 
    
    override func drawRect(rect: CGRect) {
        switch currentShapeType {
        case 0: drawCircle(90.0)
        case 1: drawCircle(10.0)
        case 2: drawCircle(360.0)
        default: print("default")
        }
    }
    
    func drawLines1() {
        // 1
        let pi:CGFloat = 22/7;
        let center = CGPoint(x:bounds.width/2, y: bounds.height/2)
        
        // 2
        let radius: CGFloat = max(bounds.width, bounds.height)
        
        // 3
        let arcWidth: CGFloat = 76
        
        // 4
        let startAngle: CGFloat = 3 * pi / 4
        let endAngle: CGFloat = pi / 4
        
        // 5
        var path = UIBezierPath(arcCenter: center,
                                radius: radius/2 - arcWidth/2,
                                startAngle: startAngle,
                                endAngle: endAngle,
                                clockwise: true)
        
        // 6
        path.lineWidth = arcWidth
        //counterColor.setStroke()
        path.stroke()
    }
    
    func drawLines() {
        //1
        let center = CGPoint(x: self.frame.size.width/2.0, y: self.frame.size.height/2.0)
        let ctx = UIGraphicsGetCurrentContext()
        
                //2
                CGContextBeginPath(ctx!)
                //CGContextMoveToPoint(ctx!, (self.frame.size.width/2.0), (self.frame.size.width - (self.frame.size.width - 10)) )
                CGContextMoveToPoint(ctx!, 0,0)
                //CGContextAddLineToPoint(ctx!, self.frame.size.width/2.0, 150.0)
                //CGContextAddLineToPoint(ctx!, self.frame.size.width/2.0, 150.0)
        
        
                //CGSize stringSize = [text sizeWithFont:font];
                //CGRect stringRect = CGRectMake(center.x-stringSize.width/2, center.y-stringSize.height/2, stringSize.width, stringSize.height);
                
                //[[UIColor blackColor] set];
                //CGContextFillRect(context, stringRect);
                
                //[[UIColor whiteColor] set];
                //[text drawInRect:stringRect withFont:font];
        
                var angle = CGFloat(90) * (22/7) / 180
                var angle1 = CGFloat(360) * (22/7) / 180
                var angle2 = CGFloat(180) * (22/7) / 180
                print("angle---\(angle)")
        
                let aFont = UIFont(name: "Arial", size: 15)
                // create a dictionary of attributes to be applied to the string
                let attr:CFDictionaryRef = [NSFontAttributeName:aFont!,NSForegroundColorAttributeName:UIColor.blackColor()]
                // create the attributed string
                let text1 = CFAttributedStringCreate(nil, "90", attr)
                let text2 = CFAttributedStringCreate(nil, "360", attr)
                let text3 = CFAttributedStringCreate(nil, "180", attr)
                // create the line of text
                let line1 = CTLineCreateWithAttributedString(text1)
                let line2 = CTLineCreateWithAttributedString(text2)
                let line3 = CTLineCreateWithAttributedString(text3)
                // retrieve the bounds of the text
        
                // set the line width to stroke the text with
                CGContextSetLineWidth(ctx!, 1.5)
                // set the drawing mode to stroke
                CGContextSetTextDrawingMode(ctx!, .Fill)
        
                var endX : CGFloat  = cos(angle) * 100 + center.x;
                var endY : CGFloat  = sin(angle) * 100 + center.y;
        
                var endX1 : CGFloat  = cos(angle1) * 100 + center.x;
                var endY1 : CGFloat  = sin(angle1) * 100 + center.y;
                
                
                var endX2 : CGFloat  = cos(angle2) * 100 + center.x;
                var endY2 : CGFloat  = sin(angle2) * 100 + center.y;
        
                var endCGPoint :CGPoint  = CGPointMake(endX, endY);
                var endCGPoint1 :CGPoint  = CGPointMake(endX1, endY1);
                var endCGPoint2 :CGPoint  = CGPointMake(endX2, endY2);
        
                CGContextSetTextMatrix(ctx!, CGAffineTransformMakeScale(1.0, -1.0))
                CGContextSetTextPosition(ctx!, endCGPoint.x, endCGPoint.y + 20)
                CTLineDraw(line1, ctx!)
                CGContextSetTextPosition(ctx!, endCGPoint1.x, endCGPoint1.y + 20)
                CTLineDraw(line2, ctx!)
                CGContextSetTextPosition(ctx!, endCGPoint2.x, endCGPoint2.y + 20)
                CTLineDraw(line3, ctx!)
        
                print("\(endCGPoint.x)-- \(endCGPoint.y)")
                CGContextAddLineToPoint(ctx!, endCGPoint.x, endCGPoint.y)
                CGContextAddLineToPoint(ctx!, endCGPoint1.x, endCGPoint1.y)
                CGContextAddLineToPoint(ctx!, endCGPoint2.x, endCGPoint2.y)
        
                CGContextSetLineWidth(ctx!, 5)
                let red = UIColor.redColor().CGColor
                CGContextSetStrokeColorWithColor(ctx!, red)
                
                //CGContextRotateCTM(ctx!, CGFloat(M_PI*90))
                
                //3
        //        CGContextClosePath(ctx!)

                CGContextStrokePath(ctx!)
    }
    
    func drawRectangle1() {
        let center = CGPointMake(self.frame.size.width / 2.0, self.frame.size.height / 2.0)
        let rectangleWidth:CGFloat = 100.0
        let rectangleHeight:CGFloat = 100.0
        
        
        let ctx = UIGraphicsGetCurrentContext()
        
        
        
        
        
        //4
        CGContextAddRect(ctx!, CGRectMake(center.x - (0.5 * rectangleWidth), center.y - (0.5 * rectangleHeight), rectangleWidth, rectangleHeight))
        CGContextSetLineWidth(ctx!, 10)
        CGContextSetStrokeColorWithColor(ctx!, UIColor.grayColor().CGColor)
        CGContextStrokePath(ctx!)
        
        //5
        CGContextSetFillColorWithColor(ctx!, UIColor.greenColor().CGColor)
        
       
        
        CGContextFillPath(ctx!)
    }
    
    func drawRectangle() {
        let center = CGPointMake(self.frame.size.width / 2.0, self.frame.size.height / 2.0)
        let rectangleWidth:CGFloat = 100.0
        let rectangleHeight:CGFloat = 100.0
        
        
        let ctx = UIGraphicsGetCurrentContext()
        
       
        let degToRed = Double(45) * (22/7) / 180
        
        print("degToRed---\(degToRed)")
        //4
        CGContextAddRect(ctx!, CGRectMake(center.x - (0.5 * rectangleWidth), center.y - (0.5 * rectangleHeight), rectangleWidth, rectangleHeight))
        CGContextSetLineWidth(ctx!, 10)
        CGContextSetStrokeColorWithColor(ctx!, UIColor.grayColor().CGColor)
        CGContextStrokePath(ctx!)
        
        //5
        CGContextSetFillColorWithColor(ctx!, UIColor.greenColor().CGColor)
        
        
        CGContextAddRect(ctx!, CGRectMake(center.x - (0.5 * rectangleWidth), center.y - (0.5 * rectangleHeight), rectangleWidth, rectangleHeight))
        
        CGContextFillPath(ctx!)
    }
    
    var ctx = UIGraphicsGetCurrentContext()
    var radius: CGFloat = 100.0
    var dragDegree : Double = 0
    
    
    func drawCircle(angles : Double) {
        print("width -- \(self.frame.size.width)--heigth--\(self.frame.size.height)")
        
        dragDegree = angles
        let center = CGPointMake(self.frame.size.width / 2.0, self.frame.size.height / 2.0);

        ctx = UIGraphicsGetCurrentContext()
        CGContextBeginPath(ctx!)
        
        //6
        CGContextSetLineWidth(ctx!, 5)
        CGContextSetStrokeColorWithColor(ctx!,UIColor.redColor().CGColor)
        //CGContextSetTextMatrix(ctx!, CGAffineTransformMakeScale(1.0, -1.0))
        
        let x:CGFloat = center.x
        let y:CGFloat = center.y
        
        let endAngle: CGFloat = CGFloat(2 * M_PI)
        CGContextAddArc(ctx!, x, y, radius, 0, endAngle, 1)
        
        //for i in 1...10 {
            CGContextSetStrokeColorWithColor(ctx!,UIColor.blackColor().CGColor)
            CGContextAddArc(ctx!, x, y, 1, 0, CGFloat(2 * M_PI), 0)
            //CGContextAddArc(ctx!, x, y, 1, 0, CGFloat(2 * M_PI), 0)
        //}
        //CGContextScaleCTM(ctx!, 1.0, -1.0)
        // save current state of context
        CGContextSaveGState(ctx!)
        
        CGContextTranslateCTM(ctx!, x, y)
        // rotation supplied in radians
        
        var angle_cal : Double
        angle_cal = 360.00 - (angles)

        
        CGContextRotateCTM(ctx!, CGFloat(M_PI*(angle_cal)/180))
        
        CGContextStrokePath(ctx!)
        
        
        //----------path
        var path = CGPathCreateMutable()
        // move to the starting point of the path
        CGPathMoveToPoint(path, nil, 0, 0)
        // add a line the length of the radius to path
        CGPathAddLineToPoint(path, nil, radius, 1)
        
        
        
        
        
        let aFont = UIFont(name: "Arial", size: 15)
        // create a dictionary of attributes to be applied to the string
        let attr:CFDictionaryRef = [NSFontAttributeName:aFont!,NSForegroundColorAttributeName:UIColor.blackColor()]
        // create the attributed string
        let text1 = CFAttributedStringCreate(nil, angles.description, attr)
        
        // create the line of text
        let line1 = CTLineCreateWithAttributedString(text1)
        
        // retrieve the bounds of the text
        
        // set the line width to stroke the text with
        CGContextSetLineWidth(ctx!, 1.5)
        // set the drawing mode to stroke
        CGContextSetTextDrawingMode(ctx!, .Fill)
        var angle = CGFloat(angles) * (22/7) / 180
        var endX : CGFloat  = cos(CGFloat(angle)) * 1 + center.x;
        var endY : CGFloat  = sin(CGFloat(angle)) * 1 + center.y;
        
        
        var endCGPoint :CGPoint  = CGPointMake(endX, endY);
        
        let tapGR = UITapGestureRecognizer(target: self, action: Selector("tapped:"))
//        let rotaGR = UIPanGestureRecognizer(target: self, action: Selector("rotated:"))
        
        self.addGestureRecognizer(tapGR)
//        self.addGestureRecognizer(rotaGR)
        
        CGContextSetTextMatrix(ctx!, CGAffineTransformMakeScale(1.0, -1.0))
        CGContextSetTextPosition(ctx!, endCGPoint.x - 35, endCGPoint.y - 115)
        CTLineDraw(line1, ctx!)
        
        
        // add the path to the (translated and rotated) context
        CGContextAddPath(ctx!, path)
        
        // set line width
        CGContextSetLineWidth(ctx!, 4)
        //CGContextFillPath(ctx!)
        
        // set line color
        CGContextSetStrokeColorWithColor(ctx!,UIColor.blueColor().CGColor)
        // stroke path
        CGContextStrokePath(ctx!)
        
        // restore original state
        CGContextRestoreGState(ctx!)
    }
    
    func tapped(sender: UITapGestureRecognizer) {
        print("---1---")
        print("---2---\(sender.locationInView(self))")
        
//        let center = CGPointMake(self.frame.size.width / 2.0, self.frame.size.height / 2.0);
//        let x:CGFloat = center.x
//        let y:CGFloat = center.y
//        
//        var path = CGPathCreateMutable()
//        // move to the starting point of the path
//                CGPathMoveToPoint(path, nil, 0, 0)
//        // add a line the length of the radius to path
//                CGPathAddLineToPoint(path, nil, radius, 1)
//        
//        dragDegree = 60.00
//        CGContextSaveGState(ctx!)
//        
//        CGContextTranslateCTM(ctx!, x, y)
//        // rotation supplied in radians
//        
//        var angle_cal : Double
//        angle_cal = 360.00 - (dragDegree)
//        
//        
//        CGContextRotateCTM(ctx!, CGFloat(M_PI*(angle_cal)/180))
//        
//        CGContextStrokePath(ctx!)
//        
//        
//        let aFont = UIFont(name: "Arial", size: 15)
//        // create a dictionary of attributes to be applied to the string
//        let attr:CFDictionaryRef = [NSFontAttributeName:aFont!,NSForegroundColorAttributeName:UIColor.blackColor()]
//        // create the attributed string
//        let text1 = CFAttributedStringCreate(nil, dragDegree.description, attr)
//        print("dragDegree---\(dragDegree)")
//        // create the line of text
//        let line1 = CTLineCreateWithAttributedString(text1)
//        
//        // retrieve the bounds of the text
//        
//        // set the line width to stroke the text with
//        CGContextSetLineWidth(ctx!, 1.5)
//        // set the drawing mode to stroke
//        CGContextSetTextDrawingMode(ctx!, .Fill)
//        var angle = CGFloat(dragDegree) * (22/7) / 180
//        var endX : CGFloat  = cos(CGFloat(angle)) * 1 + center.x;
//        var endY : CGFloat  = sin(CGFloat(angle)) * 1 + center.y;
//        
//        
//        var endCGPoint :CGPoint  = CGPointMake(endX, endY);
//        
//        
//        
//
//        
//        CGContextSetTextMatrix(ctx!, CGAffineTransformMakeScale(-1.0, 1.0))
//        CGContextSetTextPosition(ctx!, endCGPoint.x - 35, endCGPoint.y - 115)
//        CTLineDraw(line1, ctx!)
//        
//        
//        // add the path to the (translated and rotated) context
//        CGContextAddPath(ctx!, path)
//        
//        // set line width
//        CGContextSetLineWidth(ctx!, 4)
//        //CGContextFillPath(ctx!)
//        
//        // set line color
//        CGContextSetStrokeColorWithColor(ctx!,UIColor.blueColor().CGColor)
//        // stroke path
//        CGContextStrokePath(ctx!)
    }
    
//    func rotated(sender: UIPanGestureRecognizer) {
//        
//        // move to the starting point of the path
//        CGPathMoveToPoint(path, nil, 0, 0)
//        // add a line the length of the radius to path
//        CGPathAddLineToPoint(path, nil, radius, 1)
//        
//        
//        print("radius \(radius)---\(self.dragDegree)--\(dragDegree)")
////        if sender.state == .Began || sender.state == .Changed {
////            let translation = sender.translationInView(self)
////            // note: 'view' is optional and need to be unwrapped
////            sender.view!.center = CGPointMake(sender.view!.center.x + translation.x, sender.view!.center.y + translation.y)
////            sender.setTranslation(CGPointMake(0,0), inView: self)
////        }
//        
//        let aFont = UIFont(name: "Arial", size: 15)
//        // create a dictionary of attributes to be applied to the string
//        let attr:CFDictionaryRef = [NSFontAttributeName:aFont!,NSForegroundColorAttributeName:UIColor.blackColor()]
//        // create the attributed string
//        let text1 = CFAttributedStringCreate(nil, self.dragDegree.description, attr)
//        
//        // create the line of text
//        let line1 = CTLineCreateWithAttributedString(text1)
//        
//        // retrieve the bounds of the text
//        
//        // set the line width to stroke the text with
//        CGContextSetLineWidth(ctx!, 1.5)
//        // set the drawing mode to stroke
//        CGContextSetTextDrawingMode(ctx!, .Fill)
//        var angle = CGFloat(self.dragDegree) * (22/7) / 180
//        var endX : CGFloat  = cos(CGFloat(angle)) * 1 + center.x;
//        var endY : CGFloat  = sin(CGFloat(angle)) * 1 + center.y;
//        
//        
//        var endCGPoint :CGPoint  = CGPointMake(endX, endY);
//
//        
//        CGContextSetTextMatrix(ctx!, CGAffineTransformMakeScale(1.0, -1.0))
//        CGContextSetTextPosition(ctx!, endCGPoint.x - 35, endCGPoint.y - 115)
//        CTLineDraw(line1, ctx!)
//        
//        
//        // add the path to the (translated and rotated) context
//        CGContextAddPath(ctx!, path)
//        
//        // set line width
//        CGContextSetLineWidth(ctx!, 4)
//        //CGContextFillPath(ctx!)
//        
//        // set line color
//        CGContextSetStrokeColorWithColor(ctx!,UIColor.blackColor().CGColor)
//        // stroke path
//        CGContextStrokePath(ctx!)
//
//        print("---3---")
//        print("---4---\(sender.locationInView(self))")
//    }
    
}
