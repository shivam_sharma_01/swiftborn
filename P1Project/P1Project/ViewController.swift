//
//  ViewController.swift
//  P1Project
//
//  Created by FiveExceptions1 on 27/01/17.
//  Copyright © 2017 FiveExceptions1. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var currentValue: Int = 50
    
    @IBOutlet weak var totalScore: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        startNewRound()
    }

    @IBOutlet weak var slider: UISlider!
    @IBAction func sliderMoved(_ sender: UISlider) {
        currentValue = lroundf(sender.value)
        totalScore.text = "\(currentValue)"
        print("The value of the slider is now: \(currentValue)")
        
    }

    func startNewRound() {
        
        currentValue = 50
        slider.value = Float(currentValue)
    }
    
    @IBAction func AlertShow(_ sender: UIButton) {
        let alert = UIAlertController(
        title: "Hello, World",
        message: "This is value! \(currentValue)",
        preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Awesome", style: .default,
                                   handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    


}

