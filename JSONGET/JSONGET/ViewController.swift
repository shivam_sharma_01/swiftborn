import UIKit
//import SwiftyJSON

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var tableView: UITableView!
    var items = [UserObject]()
    
    override func viewWillAppear(_ animated: Bool) {
        let frame:CGRect = CGRect(x: 0, y: 100, width: self.view.frame.width, height: self.view.frame.height-100)
        self.tableView = UITableView(frame: frame)
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.view.addSubview(self.tableView)
        
        let btn = UIButton(frame: CGRect(x: 0, y: 25, width: self.view.frame.width, height: 50))
        btn.backgroundColor = UIColor.cyan
        btn.setTitle("Add new Dummy", for: UIControlState.normal)
        btn.addTarget(self, action: #selector(ViewController.addDummyData), for: UIControlEvents.touchUpInside)
        self.view.addSubview(btn)
    }
    
    func addDummyData() {
        RestAPIManager.sharedInstance.getRandomUser { (json: JSON) in
            if let results = json["results"].array {
                print(results)
                for entry in results {
                    self.items.append(UserObject(json: entry))
                }
//                DispatchQueue.main.asynchronously(execute: {
                    self.tableView.reloadData()
//                })
            }
        }
    }
    
    //MARK: UITableView DataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "CELL")
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "CELL")
        }
        let user = self.items[indexPath.row]
        
        if let url = URL(string: user.pictureURL) {
            if let data = Data(contentsOfURL: url as URL) {
                cell?.imageView?.image = UIImage(data: data)
            }
        }
        cell!.textLabel?.text = user.username
        return cell!
    }
}//end
