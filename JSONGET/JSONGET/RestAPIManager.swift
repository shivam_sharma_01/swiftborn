//
//  RestAPIManager.swift
//  RestAPIJSONProject
//
//  Created by 5Exception-Mac2 on 22/07/16.
//  Copyright © 2016 5Exception-Mac2. All rights reserved.
//

import UIKit
//import SwiftyJSON
    typealias ServiceResponse = (JSON, NSError?) -> Void
    
class RestAPIManager: NSObject {
    static let sharedInstance = RestAPIManager()
    
//    let baseURL = "https://secure.idna.works/wp-json/v2/login"
    let baseURL = "http://api.randomuser.me/"
    func getRandomUser(onCompletion: @escaping (JSON) -> Void) {
        let route = baseURL
//        let params =  ["username": "shailendraborse@yahoo.com", "password": "develop@123"]
//        makeHTTPPostRequest(path: route, body: params as [String : AnyObject] ,onCompletion: { json, err in
//            onCompletion(json as JSON)
//        })
        
                    makeHTTPGetRequest(path: route, onCompletion: { json, err in
                        onCompletion(json as JSON)
                    })
    }
    
    // MARK: Perform a GET Request
    private func makeHTTPGetRequest(path: String, onCompletion: @escaping ServiceResponse) {
        print("path--\(path)")
        let request = NSMutableURLRequest(url: NSURL(string: path)! as URL)
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let jsonData = data {
                let json:JSON = JSON(data: jsonData)
                print("json-----\(json)")
                onCompletion(json, error as NSError?)
            } else {
         
                print("error--\(data)")
                onCompletion(nil, error as NSError?)
            }
        })
        task.resume()
    }
    
    // MARK: Perform a POST Request
    private func makeHTTPPostRequest(path: String, body: [String: AnyObject], onCompletion: @escaping ServiceResponse) {
        let request = NSMutableURLRequest(url: NSURL(string: path)! as URL)
        
        // Set the method to POST
        request.httpMethod = "POST"
        
        do {
            // Set the POST body for the request
            let jsonBody = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
            request.httpBody = jsonBody
            let session = URLSession.shared
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                if let jsonData = data {
                    let json:JSON = JSON(data: jsonData)
                    //                        print("data--\(data)")
                    print("json--\(json)")
                    onCompletion(json, nil)
                } else {
                    onCompletion(nil, error as NSError?)
                }
            })
            task.resume()
        } catch {
            // Create your personal error
            onCompletion(nil, nil)
        }
    }
}
