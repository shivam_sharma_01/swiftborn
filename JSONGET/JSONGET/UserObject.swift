//
//  UserObject.swift
//  RestAPIJSONProject
//
//  Created by 5Exception-Mac2 on 22/07/16.
//  Copyright © 2016 5Exception-Mac2. All rights reserved.
//

//import SwiftyJSON

class UserObject {
    var pictureURL: String!
    var username: String!
    
    required init(json: JSON) {
        pictureURL = json["picture"]["medium"].stringValue
        username = json["email"].stringValue
    }
}
