//
//  ViewController.swift
//  staticTable
//
//  Created by FiveExceptions1 on 30/09/16.
//  Copyright © 2016 FiveExceptions1. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {
    
    var identifier_list = ["cell","cell1","cell2","cell3"]
    var storyBoard = UIStoryboard(name: "Main", bundle: nil)
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(tableView: UITableView,numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
       
        
            let cell = tableView.dequeueReusableCellWithIdentifier(identifier_list[indexPath.row], forIndexPath: indexPath)
//            cell.textLabel?.text = "/(indexPath.row)"
        
                return cell
    }
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 0
        {
            let destvc = storyBoard.instantiateViewControllerWithIdentifier("viewvc") as! NewViewController
            self.presentViewController(destvc, animated: true, completion: nil)
        }
    }
    
}

