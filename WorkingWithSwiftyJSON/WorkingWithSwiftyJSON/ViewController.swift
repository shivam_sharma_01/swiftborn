//
//  ViewController.swift
//  WorkingWithSwiftyJSON
//
//  Created by 5Exception-Mac2 on 19/08/16.
//  Copyright © 2016 5Exception-Mac2. All rights reserved.
//

import UIKit
class ViewController: UIViewController {

    @IBOutlet weak var appImageView: UIImageView!
    
    @IBOutlet var u_name: UILabel!
    
    let baseURL = "http://api.randomuser.me/"
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        forParsing()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func getDataJSON(sender: AnyObject) {
        forParsing()
    }
   
    func forParsing()
    {
        print("executing")
        let jsonURL = NSURL(string: baseURL)
        let request = NSURLRequest(URL: jsonURL!)
        let session = NSURLSession.sharedSession()
        let work = session.dataTaskWithRequest(request, completionHandler: {data,response,error -> Void in
            if let jsonData = data
            {
                let json:JSON = JSON(data:jsonData)
                if let results = json["results"].array {
                    for entry in results {
                        let email = entry["email"].stringValue
                        let gender = entry["gender"].stringValue
                        let image = entry["picture"]["medium"].stringValue
                        let imageURL = NSURL(string: image)
                        let imageData = NSData(contentsOfURL: imageURL!)
                        //self.appImageView.image = UIImage(data: imageData!)
                        let name = entry["name"]["first"].stringValue
                    dispatch_async(dispatch_get_main_queue(),{
                        print(email)
                     //   print(gender)
                    //    print(image)
                       self.appImageView.image = UIImage(data:imageData!)
                    //    print(name)
                    //    print(entry)
                        self.u_name.text = email
                    })
                    }
                }

            }else
            {
                print("error in code")
            }
        })
        work.resume()
        }
    }

