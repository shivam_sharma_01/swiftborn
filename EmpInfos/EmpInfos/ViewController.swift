//
//  ViewController.swift
//  EmpInfos
//
//  Created by FiveExceptions1 on 06/09/16.
//  Copyright © 2016 FiveExceptions1. All rights reserved.
//

import UIKit

func getDocumentsURL() -> NSURL {
    let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
    return documentsURL
}

func fileInDocumentsDirectory(filename: String) -> String {
    
    let fileURL = getDocumentsURL().URLByAppendingPathComponent(filename)
    return fileURL.path!
    
}

class ViewController: UIViewController,  UITableViewDelegate, UITableViewDataSource{

    var marrEmpData : NSMutableArray!
    @IBOutlet var MyTable: UITableView!
    @IBAction func addEmpInfo(){
    
    }
    
    override func viewWillAppear(animated: Bool) {
        self.getStudentData()
//        MyTable.reloadData()
        print("appear")
    }
    
    func getStudentData()
    {
        marrEmpData = NSMutableArray()
        marrEmpData = ModelManager.getInstance().getAllStudentData()
        MyTable.reloadData()
        for (var mData = 0 ;mData < marrEmpData.count;mData++)
        {
            //
            
            
            let randomNum = "Image-\(mData+1).png"
            
            let imagePathOne = fileInDocumentsDirectory(randomNum)
            
            
            let loadedImage = self.loadImageFromPath(imagePathOne)
            
            let temEmp:EmpInfo = marrEmpData.objectAtIndex(mData) as! EmpInfo
            if (loadedImage != nil){
                
                temEmp.photo = loadedImage!
                
            }else{
                temEmp.photo = UIImage(named: "default_user")!
            }
            
            marrEmpData.replaceObjectAtIndex(mData, withObject: temEmp)
            
        }
        
    }

    
    // These strings will be the data for the table view cells
   let animals = [UIImage(named: "images"),UIImage(named: "images1"),UIImage(named: "images2"),UIImage(named: "images3"),UIImage(named: "images4"),UIImage(named: "images5")]
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("onload")
        
        
        
        MyTable.delegate = self
        MyTable.dataSource = self
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        //return names.count;
        print("emp no \(marrEmpData.count)")
        if (marrEmpData.count > 0){
            return marrEmpData.count;
        }else{
            return 0;
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell:CustomCell = self.MyTable.dequeueReusableCellWithIdentifier("cells") as! CustomCell
        
        let emp:EmpInfo = marrEmpData.objectAtIndex(indexPath.row) as! EmpInfo
    
        
        

        // user profile
        cell.name.text = emp.full_name
        cell.age.text = emp.age
        cell.phone.text = emp.phone
        cell.photo.image = emp.photo
        
        
        return cell
    }
    
    /*let randomNum = "Image-\(indexPath.row+1).png"
    
    let imagePathOne = fileInDocumentsDirectory(randomNum)
    
    
    let loadedImage = self.loadImageFromPath(imagePathOne)*/
    
    
    //
    
    // 1
    /* let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
    dispatch_async(queue) {
    
    
    dispatch_async(dispatch_get_main_queue()) {
    // update the user interface
    if  ((loadedImage) != nil)  {
    print("---\(loadedImage)")
    
    // 3
    
    return
    }
    else{
    print("alt")
    cell.photo.image = UIImage(named: "default_user")
    }
    }
    print("Error!")
    }*/
    //        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
    //        dispatch_async(dispatch_get_global_queue(priority, 0)) {
    //            // do some task
    //            dispatch_async(dispatch_get_main_queue()) {
    //                // update some UI
    //                if  ((loadedImage) != nil) {
    //                    print("view Loaded Image: \(loadedImage)")
    //                    cell.photo.image = loadedImage
    //                }
    //                else {
    //                    cell.photo.image = UIImage(named: "default_user")
    //                }
    //            }
    //        }

    
    // which one is tapped
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }
    
    // load image of user
    func loadImageFromPath(path: String) -> UIImage? {
        print("image-----\(path)")
        let image = UIImage(contentsOfFile: path)
        
        if image == nil {
            
            print("missing image at: \(path)")
            return nil
        }


        print("Loading image from path: \(path)") // this is just for you to see the path in case you want to go to the directory, using Finder.
        return image
    }
    
}

