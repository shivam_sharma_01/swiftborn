//
//  ModelManager.swift
//  DataBaseDemo
//
//  Created by Krupa-iMac on 05/08/14.
//  Copyright (c) 2014 TheAppGuruz. All rights reserved.
//

import UIKit

let sharedInstance = ModelManager()

class ModelManager: NSObject {
    
    var database: FMDatabase? = nil

    class func getInstance() -> ModelManager
    {
        if(sharedInstance.database == nil)
        {
            sharedInstance.database = FMDatabase(path: Util.getPath("EmpInfo_DB3.sqlite"))
        }
        return sharedInstance
    }
    
    func addStudentData(empInfo: EmpInfo) -> Bool {
        sharedInstance.database!.open()
        let isInserted = sharedInstance.database!.executeUpdate("INSERT INTO profile (full_name, address, phone, age) VALUES (?, ?, ?, ?)", withArgumentsInArray: [empInfo.full_name, empInfo.address, empInfo.phone, empInfo.age])
        sharedInstance.database!.close()
        return isInserted
    }
   
    func updateStudentData(empInfo: EmpInfo) -> Bool {
        sharedInstance.database!.open()
        let isUpdated = sharedInstance.database!.executeUpdate("UPDATE profile SET full_name=?, address=?, age=?, phone=? WHERE id=?", withArgumentsInArray: [empInfo.full_name, empInfo.address, empInfo.age, empInfo.phone, empInfo.id])
        sharedInstance.database!.close()
        return isUpdated
    }
    
    func deleteStudentData(empInfo: EmpInfo) -> Bool {
        sharedInstance.database!.open()
        let isDeleted = sharedInstance.database!.executeUpdate("DELETE FROM profile WHERE id=?", withArgumentsInArray: [empInfo.id])
        sharedInstance.database!.close()
        return isDeleted
    }

    func getAllStudentData() -> NSMutableArray {
        sharedInstance.database!.open()
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT * FROM profile", withArgumentsInArray: nil)
        let marrEmpInfo : NSMutableArray = NSMutableArray()
        if (resultSet != nil) {
            while resultSet.next() {
                let empInfo : EmpInfo = EmpInfo()
//                print("hai "+resultSet.stringForColumn("age"))
                empInfo.id = resultSet.stringForColumn("id")
                empInfo.full_name = resultSet.stringForColumn("full_name")
                empInfo.address = resultSet.stringForColumn("address")
                empInfo.age = resultSet.stringForColumn("age")
                empInfo.phone = resultSet.stringForColumn("phone")
                
                marrEmpInfo.addObject(empInfo)
            }
        }
        sharedInstance.database!.close()
        return marrEmpInfo
    }
    
}
