//
//  AddEmpViewController.swift
//  EmpInfos
//
//  Created by FiveExceptions1 on 07/09/16.
//  Copyright © 2016 FiveExceptions1. All rights reserved.
//

import UIKit

func getDocumentsURLAdd() -> NSURL {
    let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
    return documentsURL
}

func fileInDocumentsDirectoryAdd(filename: String) -> String {
    
    let fileURL = getDocumentsURLAdd().URLByAppendingPathComponent(filename)
    return fileURL.path!
    
}


class AddEmpViewController: UITableViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    
   // for content
    
    var marrEmpDataInfo : NSMutableArray!
    
    @IBOutlet var addFullName: UITextField!
    @IBOutlet var addAddress: UITextField!

    @IBOutlet var addPhone: UITextField!

    @IBOutlet var dataME: UITextField!
    @IBOutlet var imagePic: UIImageView!
    
    @IBOutlet var imageLocal: UIImageView!

    // actionsheet option for image view
    @IBAction func showActionSheet(sender: AnyObject) {
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .ActionSheet)
        // 1
        let cameraAction = UIAlertAction(title: "Camera", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("cameraAction")
            self.getImageFromCamera()
        })
        
        // 2
        let galleryAction = UIAlertAction(title: "Gallery", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Gallery")
            self.getImageFromGallery()
        })
        
        // 3
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        
        // 4
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
    
    // take image from library
    func getImageFromGallery(){
        print("hai ")
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        self.presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    // take image from camera
    func getImageFromCamera(){
        print("hai camera")
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
        self.presentViewController(imagePicker, animated: true, completion: nil)
    }

    // imagepicker controller
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let theInfo:NSDictionary = info as NSDictionary
        let imageSource:UIImage = theInfo[UIImagePickerControllerOriginalImage] as! UIImage
        
        marrEmpDataInfo = NSMutableArray()
        marrEmpDataInfo = ModelManager.getInstance().getAllStudentData()
        
        print("count  \(marrEmpDataInfo.count) ")
        
        imagePic.image = imageSource
        //let randomNum = "Image-"+"\(String(RandomInt(9999)))"+".png"
        
        let randomNum = "Image-\(marrEmpDataInfo.count+1).png"
        
        let imagePathOne = fileInDocumentsDirectoryAdd(randomNum)
        
        if let image = imagePic.image {
            saveImage(image, path: imagePathOne)
        } else { print("1 some error message") }
        
        if let loadedImage = loadImageFromPath(imagePathOne) {
            print(" Loaded Image: \(loadedImage)")
            imagePic.image = loadedImage
        } else {
            let loadedImage1 = loadImageFromPath(imagePathOne)
            print("2 some error message 2 \(loadedImage1)")
            
        }
        
        
        
        //print("imageSource \(imageName) "+" hai  \(imagePath) "+"new  \(imagePathOne)")
        //print("randomNum \(randomNum) "+" ---- \(imageSource)")
        
        print("randomNum \(randomNum) ")
        
        
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    func saveImage (image: UIImage, path: String ) -> Bool{
        
        let pngImageData = UIImagePNGRepresentation(image)
        //let jpgImageData = UIImageJPEGRepresentation(image, 1.0)   // if you want to save as JPEG
        let result = pngImageData!.writeToFile(path, atomically: true)
        print("result \(result)")
        return result
        
    }
    
    func loadImageFromPath(path: String) -> UIImage? {
        
        let image = UIImage(contentsOfFile: path)
        
        if image == nil {
            
            print("missing image at: \(path)")
            
        }
        imageLocal.image = image
        print("Loading image from path: \(path)") // this is just for you to see the path in case you want to go to the directory, using Finder.
        return image
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func RandomInt(max: Int) ->  Int {
        
        return Int( arc4random_uniform( UInt32(max) ) ) + 0
    }

    // to cancel add emp info add page
    @IBAction func cancel(){
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    // to done add emp info
    @IBAction func done(){
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    // text which call the datepicker
    @IBAction func dateTextInputPressed(sender: UITextField) {
        print("date enter")
        //Create the view
        let inputView = UIView(frame: CGRectMake(0, 0, self.view.frame.width, 240))
        
        
        let datePickerView  : UIDatePicker = UIDatePicker(frame: CGRectMake(0, 40, 0, 0))
        datePickerView.datePickerMode = UIDatePickerMode.Date
        inputView.addSubview(datePickerView) // add date picker to UIView
        
        let doneButton = UIButton(frame: CGRectMake((self.view.frame.size.width/2) - (100/2), 0, 100, 50))
        doneButton.setTitle("Done", forState: UIControlState.Normal)
        doneButton.setTitle("Done", forState: UIControlState.Highlighted)
        doneButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        doneButton.setTitleColor(UIColor.grayColor(), forState: UIControlState.Highlighted)
        
        inputView.addSubview(doneButton) // add Button to UIView
        
        doneButton.addTarget(self, action: "doneButton:", forControlEvents: UIControlEvents.TouchUpInside) // set button click event
        
        sender.inputView = inputView
        datePickerView.addTarget(self, action: Selector("handleDatePicker:"), forControlEvents: UIControlEvents.ValueChanged)
        
        handleDatePicker(datePickerView) // Set the date on start.
    }
    
    func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        dataME.text = dateFormatter.stringFromDate(sender.date)
    }
    
    func doneButton(sender:UIButton)
    {
        dataME.resignFirstResponder() // To resign the inputView on clicking done.
    }
    
    // submit emp info
    @IBAction func submitInfo(sender: AnyObject) {
        let empInfo: EmpInfo = EmpInfo()
        if(addFullName.text == "")
        {
            Util.invokeAlertMethod("", strBody: "Please enter fuul name name.", delegate: nil)
        }
        else if(addAddress.text == "")
        {
            Util.invokeAlertMethod("", strBody: "Please enter address marks.", delegate: nil)
        }
        else if(addPhone.text == "")
        {
            Util.invokeAlertMethod("", strBody: "Please enter phone number marks.", delegate: nil)
        }
        else
        {
            empInfo.full_name = addFullName.text!
            empInfo.address = addAddress.text!
            empInfo.age = dataME.text!
            empInfo.phone = addPhone.text!
        
            let isUpdated = ModelManager.getInstance().addStudentData(empInfo)
            if isUpdated {
                Util.invokeAlertMethod("", strBody: "Record updated successfully.", delegate: nil)
//                dismissViewControllerAnimated(true, completion: nil)
            } else {
                Util.invokeAlertMethod("", strBody: "Error in updating record.", delegate: nil)
//                dismissViewControllerAnimated(true, completion: nil)
            }
            
        }
    }

}
