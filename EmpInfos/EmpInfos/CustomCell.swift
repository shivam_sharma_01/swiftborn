//
//  CustomCell.swift
//  EmpInfos
//
//  Created by FiveExceptions1 on 07/09/16.
//  Copyright © 2016 FiveExceptions1. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {

    @IBOutlet weak var photo: UIImageView!
    @IBOutlet var name: UILabel!
    
    @IBOutlet var age: UILabel!
    
    @IBOutlet var phone: UILabel!
//    @IBOutlet var age: UILabel!
//    @IBOutlet var phone: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
