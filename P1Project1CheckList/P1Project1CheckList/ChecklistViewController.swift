//
//  ViewController.swift
//  P1Project1CheckList
//
//  Created by FiveExceptions1 on 28/01/17.
//  Copyright © 2017 FiveExceptions1. All rights reserved.
//

import UIKit

class ChecklistViewController: UITableViewController {
    
    var row0text = "let me hear what you say"
    var row1text = "I came by to tell you"
    var row2text = "you won't let your children see you die"
    var row3text = "Ever since I see around"
    var row4text = "one person meet another"
    
    var row0checked = false
    var row1checked = false
    var row2checked = false
    var row3checked = false
    var row4checked = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(_ tableView : UITableView , numberOfRowsInSection section : Int) -> Int{
        return 5
    }
    
    
    
    override func tableView(_ tableView: UITableView,cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChecklistItem", for: indexPath as IndexPath)
        
        let label = cell.viewWithTag(1000) as! UILabel
        
        if indexPath.row == 0 {
            label.text = row0text
        }else if indexPath.row == 1 {
            label.text = row1text
        }else if indexPath.row  == 2{
            label.text = row2text
        }else if indexPath.row  == 3{
            label.text = row3text
        }else if indexPath.row  == 4{
            label.text = row4text
        }
        
        configureCheckmarkForCell(cell: cell, indexPath: indexPath as NSIndexPath)
        
        return cell
    }
    
    
    func configureCheckmarkForCell(cell : UITableViewCell , indexPath : NSIndexPath){
        var isChecked = false
        if indexPath.row == 0 {
            isChecked = row0checked
        } else if indexPath.row == 1 {
            isChecked = row1checked
        } else if indexPath.row == 2 {
            isChecked = row2checked
        } else if indexPath.row == 3 {
            isChecked = row3checked
        } else if indexPath.row == 4 {
            isChecked = row4checked
        }
        if isChecked {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let cell = tableView.cellForRow(at: indexPath){
            
            var isChecked = false
            
            if indexPath.row == 0 {
                row0checked = !row0checked
                isChecked = row0checked
                
            }
            else if indexPath.row == 1 {
                row1checked = !row1checked
                isChecked = row1checked
                            }
            else if indexPath.row == 2 {
                row2checked = !row2checked
                isChecked = row2checked
                
            }
            else if indexPath.row == 3 {
                row3checked = !row3checked
                isChecked = row3checked
                
            }
            else if indexPath.row == 4 {
                row4checked = !row4checked
                isChecked = row4checked
                
            }
            
            if isChecked{
                cell.accessoryType = .checkmark
            }else {
                cell.accessoryType = .none
            }
            
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

