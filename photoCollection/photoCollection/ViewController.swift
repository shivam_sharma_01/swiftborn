//
//  ViewController.swift
//  photoCollection
//
//  Created by FiveExceptions1 on 13/09/16.
//  Copyright © 2016 FiveExceptions1. All rights reserved.
//

import UIKit

class CollectionViewController: UICollectionViewController {

    var Array = [String]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        Array = ["images","images1","images2","images3","images4","images5","images","images1","images2","images3","images4","images5"]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
     //   <#code#>
        return Array.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cells", forIndexPath: indexPath) as! File
                
        cell.imageView.image = UIImage(named: Array[indexPath.row])
        
        return cell
    }
}

