//
//  ReferAppViewController.swift
//  TabViewExample
//
//  Created by FiveExceptions1 on 24/09/16.
//  Copyright © 2016 FiveExceptions1. All rights reserved.
//

import UIKit
import MessageUI
import Social

class ReferAppViewController: UIViewController, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate {
    // share now
    @IBAction func referApp(_ sender: AnyObject) {
        
        let optionMenu = UIAlertController(title: nil, message: "Share", preferredStyle:UIAlertControllerStyle.actionSheet)
     
        let messageAction = UIAlertAction(title: "Message", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let messageVC = MFMessageComposeViewController()
            
            messageVC.body = "Enter a message";
            messageVC.recipients = []
            messageVC.messageComposeDelegate = self;
            
            self.present(messageVC, animated: false, completion: nil)
            print("Message")
            //self.getImageFromCamera()
        })
  
        let emailAction = UIAlertAction(title: "Email", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let picker = MFMailComposeViewController()
            picker.mailComposeDelegate = self
            picker.setSubject("Invite for Family Food app.")
            picker.setMessageBody("Hello every one please join us to get delicious food", isHTML: false)
            
            self.present(picker, animated: true, completion: nil)
            
            print("Email")
            //self.getImageFromGallery()
        })
        
        let twitterAction = UIAlertAction(title: "Twitter", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeTwitter) {
                // 2
                let tweetSheet = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
                // 3
                tweetSheet?.setInitialText("Look at this nice picture!")
                //tweetSheet.addImage(imageView.image)
                
                // 4
                self.present(tweetSheet!, animated: true, completion: nil)
            } else {
                // 5
                print("twitter error")
            }
            
            print("Twitter")
            //self.getImageFromCamera()
        })
        
        let facebookAction = UIAlertAction(title: "Facebook", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook) {
                // 2
                let controller = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
                // 3
                controller?.setInitialText("Testing Posting to Facebook")
                // 4
                self.present(controller!, animated:true, completion:nil)
            }
            else {
                // 3
                print("no Facebook account found on device")
            }
            
            print("Facebook")
            //self.getImageFromGallery()
        })
        
      
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
      
        optionMenu.addAction(messageAction)
        optionMenu.addAction(emailAction)
        optionMenu.addAction(twitterAction)
        optionMenu.addAction(facebookAction)
     
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
//        case MessageComposeResultCancelled:
//            print("Message was cancelled")
//            self.dismiss(animated: true, completion: nil)
//        case MessageComposeResultFailed:
//            print("Message failed")
//            self.dismiss(animated: true, completion: nil)
//        case MessageComposeResultSent:
//            print("Message was sent")
//            self.dismiss(animated: true, completion: nil)
        default:
            break;
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        dismiss(animated: true, completion: nil)
    }
}
