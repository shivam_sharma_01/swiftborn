//
//  ViewController.swift
//  simplyExample
//
//  Created by FiveExceptions1 on 20/09/16.
//  Copyright © 2016 FiveExceptions1. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {
    
    
    @IBOutlet var myTable: UITableView!
    let cellSpacingHeight: CGFloat = 5
    var locations = ["asian","banting","breakfast","burger","child friendly","cookery classes","coffee",
        "dersert","drink","indian","italian","luxury dining","meat lover","mexican","nightlife","pet friendly","recipes","seafood","streetfood","sushi","vegetarian","vegan"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.tableView.sectionHeaderHeight = 290
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    //        return 5
    //    }
    
    override func tableView(_ tableView: UITableView,numberOfRowsInSection section: Int) -> Int {
            return locations.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 150 //cell height
        //        if indexPath.row % 2 == 0
        //        {
        //            return 100 //cell height
        //        }
        //        else
        //        {
        //            return 5 //space heigh
        //        }
    }
    
//    override func tableView(tableView: UITableView,willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
//            return nil
//    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0,width: tableView.frame.size.width, height: 40))
        headerView.backgroundColor = UIColor.white
        headerView.isOpaque = true
        headerView.alpha = 0.8

        //headerView.imageView?.image = UIImage(named: "news")

        //news
        let buttonImage = UIButton(type: .system)
        let image1Name = UIImage(named: "contact")
        
        //serach 
        let buttonImageSerach = UIButton(type: .system)
        let image2Name = UIImage(named: "news")
        //buttonImage.setTitle("Hai:", forState: UIControlState.Normal)
        
        // mesage image
        let imageMsgName = "ic_logo"
        let imageMsg = UIImage(named: imageMsgName)
        let imageView = UIImageView(image: imageMsg!)
        imageView.contentMode = UIViewContentMode.scaleAspectFit;
        
        // nearby image
        let imageNearByName = "ic_action_nearbyactive"
        let imageNearBy = UIImage(named: imageNearByName)
        let imageViewNearBy = UIImageView(image: imageNearBy!)
        imageViewNearBy.contentMode = UIViewContentMode.scaleAspectFit;
        
        
        //Finally you'll need to give imageView a frame and add it your view for it to be visible:
        
        imageView.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        imageViewNearBy.frame = CGRect(x: tableView.frame.size.width-40, y: 90, width: 32, height: 32)
        
        buttonImage.setImage(image1Name, for: UIControlState())
        buttonImage.frame = CGRect(x: tableView.frame.size.width-40 , y: 15, width: 32, height: 32)
        buttonImage.tag = 1
        buttonImage.addTarget(self, action: #selector(ViewController.btnTouched(_:)), for:.touchUpInside)
        
        
        // search
        buttonImageSerach.setImage(image2Name, for: UIControlState())
        buttonImageSerach.frame = CGRect(x: 0 , y: 90, width: 32, height: 32)
        buttonImageSerach.tag = 2
        buttonImageSerach.addTarget(self, action: #selector(ViewController.searchLocations(_:)), for:.touchUpInside)
        
        headerView.addSubview(imageView)
        headerView.addSubview(imageViewNearBy)
        
        headerView.addSubview(buttonImageSerach)
        headerView.addSubview(buttonImage)
        
        return headerView
    }
    
    func btnTouched(_ sender: UIButton!)
    {
        if(sender.tag == 1){
            print("messages")
            self.performSegue(withIdentifier: "goToMessageView", sender: nil)
        }
    }
    
    func searchLocations(_ sender: UIButton!)
    {
        if(sender.tag == 2){
            print("serach location")
            self.performSegue(withIdentifier: "goToSearchLocationView", sender: nil)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 120.0
    }
    
    override func tableView(_ tableView: UITableView,
        cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let cell:CustomCell = self.myTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CustomCell
            
            cell.categories.backgroundColor = UIColor.red
            
            cell.categories.text = " "+(locations[indexPath.row])
            return cell
    }
    
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        segue.destinationViewController.navigationItem.title = "Messages"
//    }
    
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    
//       self.navigationController?.navigationBar.backItem?.title = "TEXT"
//        let vc =  segue.destinationViewController as! Messages
        //vc.titleOfVC1 = "Home"
        
//    }
    
}
    
    //    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    //        return 140.0
    //    }
    
    //    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //        return 40
    //    }
    
    


