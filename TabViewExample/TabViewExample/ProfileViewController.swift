//
//  ProfileViewController.swift
//  TabViewExample
//
//  Created by FiveExceptions1 on 24/09/16.
//  Copyright © 2016 FiveExceptions1. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    
    @IBOutlet var myProfileTable: UITableView!
    let profileList:[String] = ["Registration","Notifications"]
     func tableView(_ tableView: UITableView,numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
     func tableView(_ tableView: UITableView,
        cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
            
            let cell = myProfileTable.dequeueReusableCell(withIdentifier: profileList[indexPath.row], for: indexPath) 
            return cell
    }
}
