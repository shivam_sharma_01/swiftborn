//
//  MapViewController.swift
//  TabViewExample
//
//  Created by FiveExceptions1 on 24/09/16.
//  Copyright © 2016 FiveExceptions1. All rights reserved.
//
import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController , MKMapViewDelegate{
    
    @IBOutlet var mapView: MKMapView!
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        super.viewWillDisappear(animated)
    }    
    
    let locationManager = CLLocationManager()
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
//        locationManager.requestWhenInUseAuthorization()
//        locationManager.startUpdatingLocation()
        struct Location {
            let title: String
            let latitude: Double
            let longitude: Double
        }
        
        let locations = [
            Location(title: "New York, NY",    latitude: 22.7196,longitude: 75.8577),
            Location(title: "Los Angeles, CA", latitude: 22.1196,longitude: 75.177),
            Location(title: "Chicago, IL",     latitude: 22.2196,longitude: 75.577)
        ]
        
        for location in locations {
            let annotation = MKPointAnnotation()
            annotation.title = location.title
            
            annotation.coordinate = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
            
//            let span = MKCoordinateSpanMake(0.1, 0.1)
//            let region = MKCoordinateRegion(center: location, span: span)
//            mapView.setRegion(region, animated: true)
            
            mapView.showsPointsOfInterest = true
            mapView.showsUserLocation = true
            mapView.addAnnotation(annotation)
        }
        
//        let location = CLLocationCoordinate2D(latitude: 22.7196,longitude: 75.8577)
//        
//        let span = MKCoordinateSpanMake(0.1, 0.1)
//        let region = MKCoordinateRegion(center: location, span: span)
//        mapView.setRegion(region, animated: true)
//        mapView.showsPointsOfInterest = true
//        mapView.showsUserLocation = true
        
//        let annotation = MKPointAnnotation()
//        
//        annotation.coordinate = location
//        
//        annotation.title = "Indore-------------"
//        
//        annotation.subtitle = "Center of India"
//        
//
//        
//        mapView.addAnnotation(annotation)
        
        //displayMarkers()
    }
    
    // When user taps on the disclosure button you can perform a segue to navigate to another view controller
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if control == view.rightCalloutAccessoryView{
            print("call right")
            print(view.annotation!.title) // annotation's title
            print(view.annotation!.subtitle) // annotation's subttitle
            print(view.rightCalloutAccessoryView?.tag)
            
            if view.rightCalloutAccessoryView?.tag == 5{
                self.performSegue(withIdentifier: "pinToView", sender: nil)
                
//                let yourNextViewController = (self.destinationViewController as LocationViewController)
//                yourNextViewController.foodTitle = view.annotation!.title
            }
            
            
            //Perform a segue here to navigate to another viewcontroller
            // On tapping the disclosure button you will get here
        }
    }
    
    // Here we add disclosure button inside annotation window
//    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
//        
//        print("viewForannotation")
//        if annotation is MKUserLocation {
//            //return nil
//            return nil
//        }
//        
//        let reuseId = "pin"
//        var pinView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId) as? MKPinAnnotationView
//        
//        if pinView == nil {
//            print("Pinview was nil")
//            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
//            pinView!.canShowCallout = true
//            pinView!.animatesDrop = true
//            pinView!.image = UIImage(named: "news")
//        }
//        
//        let button = UIButton(type: .DetailDisclosure) as UIButton // button with info sign in it
//        pinView?.canShowCallout = true
//        pinView?.image = UIImage(named: "news")
//        pinView?.rightCalloutAccessoryView = button
//        
//        
//        return pinView
//    }
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let annotationReuseId = "Place"
        
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationReuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationReuseId)
        } else {
            anView!.annotation = annotation
        }
        
        anView!.image = UIImage(named: "cal")
        //anView!.backgroundColor = UIColor.clearColor()

        
        let button = UIButton(type: .detailDisclosure) as UIButton // button with info sign in it
        button.tag = 5
        button.frame = CGRect(x: (anView?.frame.width)!, y: ((anView?.frame.height)! - 5), width: 50, height: 50)
        //button.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        anView?.canShowCallout = true

        anView?.rightCalloutAccessoryView = button
        
        let leftView = UIImageView(image: UIImage(named: "cal"))
        leftView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        //leftView.backgroundColor = UIColor.redColor()
        anView!.leftCalloutAccessoryView = leftView
        
        
        return anView!
    }
    
    func displayMarkers() -> Void
    {
       
        print("displayMarkers")
        let annotationView = MKAnnotationView()
        
        // Adding button here wont do anything  so remove these two lines
        
        
        let detailButton: UIButton = UIButton(type: .detailDisclosure) as UIButton
        annotationView.image = UIImage(named: "news")
        annotationView.leftCalloutAccessoryView = detailButton
        
        
        let annotation = MKPointAnnotation()
                
    
                
        annotation.title = "Hai"
        annotation.subtitle = "Hai"
     
        
        
        mapView.addAnnotation(annotation)
        
       
    }


}
