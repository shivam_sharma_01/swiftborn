//
//  RestaurantInfo.swift
//  TabViewExample
//
//  Created by FiveExceptions1 on 28/09/16.
//  Copyright © 2016 FiveExceptions1. All rights reserved.
//

import UIKit

class RestaurantInfo: UITableViewCell {

    //labels
    @IBOutlet var detail1: UILabel!
    @IBOutlet var detail2: UILabel!
    @IBOutlet var detail3: UILabel!
    @IBOutlet var detail4: UILabel!
    @IBOutlet var detail5: UILabel!
    
    //images
    
    @IBOutlet var d_img1: UIImageView!
    @IBOutlet var d_img2: UIImageView!
    @IBOutlet var d_img3: UIImageView!
    @IBOutlet var d_img4: UIImageView!
    @IBOutlet var d_img5: UIImageView!
    
    
    
    @IBOutlet var locationImage: UIImageView!
    
    @IBOutlet var locationFoodName: UILabel!
}
