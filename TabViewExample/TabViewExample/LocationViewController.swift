//
//  LocationViewController.swift
//  TabViewExample
//
//  Created by FiveExceptions1 on 26/09/16.
//  Copyright © 2016 FiveExceptions1. All rights reserved.
//

import UIKit


class LocationViewController : UIViewController,UITableViewDelegate,UITableViewDataSource{
    //<#properties and methods#>
    
    @IBOutlet var locationTableView: UITableView!
    var detailInfo = ["View Menu","Show Specials","Restaurant Info","Rate Restaurant","Book a table"]
    
    var detailInfoIcon = ["ic_action_menu","ic_action_prizesactive","ic_action_info","ic_action_profileactive","ic_action_calender"]
    var identifier_list = ["locationLogo","foodName","locationInfo","locationInfo1","locationInfo2","locationInfo3","locationInfo4"]
    
    override func viewDidLoad()
    {
        navigationController!.navigationBar.barTintColor = UIColor.gray
       
    }
   
    
    @IBAction func doneRestaurant(_ sender: AnyObject) {
        print("doneRestaurant")
        //self.dismissViewControllerAnimated(true, completion: nil)
        self.navigationController?.popViewController(animated: false)

    }
   
    
       // print("doneRestaurant")
      //  self.dismissViewControllerAnimated(true, completion: nil)
    
    func tableView(_ tableView: UITableView,numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    
//    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
//            var cell = RestaurantInfo()
//            if indexPath.row == 0
//            {
//             cell = locationTableView.dequeueReusableCellWithIdentifier(identifier_list[indexPath.row], forIndexPath: indexPath) as! RestaurantInfo
//                
//            cell.locationImage.image = UIImage(named: "foodicon")
//                
//            }
//        else if indexPath.row == 1
//        {
//            cell = locationTableView.dequeueReusableCellWithIdentifier(identifier_list[indexPath.row], forIndexPath: indexPath) as! RestaurantInfo
//            cell.locationFoodName.text = "Maadio"
//        }
//        else if indexPath.row > 1 && indexPath.row < 7  {
//            cell = locationTableView.dequeueReusableCellWithIdentifier(identifier_list[2], forIndexPath: indexPath) as! RestaurantInfo
//            
//            cell.restaurantDetailsLabel.text = detailInfo[indexPath.row-2]
//            
//            cell.detailsIcon.image = UIImage(named: detailInfoIcon[indexPath.row - 2])
//        }
//        return cell
//    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = RestaurantInfo()
        // cell 1
        if indexPath.row == 0
        {
            cell = locationTableView.dequeueReusableCell(withIdentifier: identifier_list[indexPath.row], for: indexPath) as! RestaurantInfo
            
            cell.locationImage.image = UIImage(named: "foodicon")
            
        }
        // cell 2
        else if indexPath.row == 1
        {
            cell = locationTableView.dequeueReusableCell(withIdentifier: identifier_list[indexPath.row], for: indexPath) as! RestaurantInfo
            cell.locationFoodName.text = "Maadio"
        }
        // cell 3
        else if indexPath.row > 1 && indexPath.row < 3  {
            cell = locationTableView.dequeueReusableCell(withIdentifier: identifier_list[2], for: indexPath) as! RestaurantInfo
            
            if (indexPath.row-2)  == 0{
                cell.detail1.text = "View Menu"
                cell.d_img1.image = UIImage(named: "ic_action_menu")
            }
        }
        // cell 4
        else if indexPath.row > 2 && indexPath.row < 4  {
            cell = locationTableView.dequeueReusableCell(withIdentifier: identifier_list[3], for: indexPath) as! RestaurantInfo
            if (indexPath.row-2) == 1{
                cell.detail2.text = "Show Specials"
                cell.d_img2.image = UIImage(named: "ic_action_prizesactive")
            }
        }
        // cell 5
        else if indexPath.row > 3 && indexPath.row < 5  {
            cell = locationTableView.dequeueReusableCell(withIdentifier: identifier_list[4], for: indexPath) as! RestaurantInfo
            if (indexPath.row-2) == 2{
                cell.detail3.text = "Restaurant Info"
                cell.d_img3.image = UIImage(named: "ic_action_info")
            }
        }
        // cell 6
        else if indexPath.row > 4 && indexPath.row < 6  {
            cell = locationTableView.dequeueReusableCell(withIdentifier: identifier_list[5], for: indexPath) as! RestaurantInfo
            if (indexPath.row-2) == 3{
                cell.detail4.text = "Rate Restaurant"
                cell.d_img4.image = UIImage(named: "ic_action_profileactive")
            }
        }
        // cell 7
        else if indexPath.row > 5 && indexPath.row < 7  {
            cell = locationTableView.dequeueReusableCell(withIdentifier: identifier_list[6], for: indexPath) as! RestaurantInfo
            if (indexPath.row-2) == 4{
                cell.detail5.text = "Book a table"
                cell.d_img5.image = UIImage(named: "ic_action_calender")
            }
        }
            //cell.restaurantDetailsLabel.text = detailInfo[indexPath.row-2]

            //cell.detailsIcon.image = UIImage(named: detailInfoIcon[indexPath.row-2])
            //return cells
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("indexPath.row  \(indexPath.row)")
//        if (indexPath.row-2)  == 0{
//            performSegueWithIdentifier("location1", sender: nil)
//        }
//        else if (indexPath.row-2) == 1{
//            performSegueWithIdentifier("location2", sender: nil)
//        }
//        else if (indexPath.row-2) == 2{
//            performSegueWithIdentifier("location3", sender: nil)
//        }
        
        /*else if (indexPath.row-2) == 3{
            performSegueWithIdentifier("location4", sender: nil)
        }
        else if (indexPath.row-2) == 4{
            performSegueWithIdentifier("location5", sender: nil)
        }*/
    }

}
