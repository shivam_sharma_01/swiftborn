//
//  BookTable.swift
//  TabViewExample
//
//  Created by FiveExceptions1 on 03/10/16.
//  Copyright © 2016 FiveExceptions1. All rights reserved.
//

import UIKit

class BookTable : UIViewController{
    
    
    @IBOutlet var timeText: UITextField!
    
    @IBOutlet var dateText: UITextField!
   
    @IBAction func dateSetter(_ sender: UITextField) {
         print("dateaction")
        
        print("UITapGestureRecognizer")
        //Create the view
        let inputView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 240))
        
        let inputButtonView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 40))
        inputButtonView.backgroundColor = UIColor.gray
        inputView.addSubview(inputButtonView) // add UIView inside UIView
        
        let datePickerView  : UIDatePicker = UIDatePicker(frame: CGRect(x: (self.view.frame.width/3) - 100, y: 40, width: 0, height: 0))
        datePickerView.datePickerMode = UIDatePickerMode.date
      
        inputView.addSubview(datePickerView) // add date picker to UIView

        print("----\(self.view.frame.size.width)")
        
//        let image1 = UIImage(named: "foodicon.png")! as UIImage
//        let image2 = UIImage(named: "halal.png")! as UIImage

        // done button-----------
        let doneButton = UIButton(frame: CGRect(x: 10, y: 0, width: 50, height: 40))
        
        
        doneButton.setTitle("Done", for: UIControlState())
        doneButton.setTitle("Done", for: UIControlState.highlighted)
        doneButton.setTitleColor(UIColor.black, for: UIControlState())
        doneButton.setTitleColor(UIColor.gray, for: UIControlState.highlighted)
        doneButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        doneButton.tag = 12
//        doneButton.setBackgroundImage(image1, forState: UIControlState.Normal)

        inputView.addSubview(doneButton) // add Button to UIView

        doneButton.addTarget(self, action: #selector(BookTable.doneButton(_:)), for: UIControlEvents.touchUpInside) // set button click event
        
        let titleLable = UILabel(frame: CGRect( x: (self.view.frame.width/2) - ((100/2)-20), y: 0,width: 60, height: 40 ) )
        titleLable.text = "Date"
        titleLable.textColor = UIColor.white
        titleLable.font = UIFont.boldSystemFont(ofSize: 20.0)
        titleLable.tag = 13
        
        inputView.addSubview(titleLable) // add Label to UIView
        
        // cancel-----------
        let cancelButton = UIButton(frame: CGRect(x: self.view.frame.width - 80, y: 0,width: 70, height: 40))
        cancelButton.setTitle("Cancel", for: UIControlState())
        cancelButton.setTitle("Cancel", for: UIControlState.highlighted)
        cancelButton.setTitleColor(UIColor.black, for: UIControlState())
        cancelButton.setTitleColor(UIColor.gray, for: UIControlState.highlighted)
        cancelButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        cancelButton.tag = 14
//        cancelButton.setBackgroundImage(image2, forState: UIControlState.Normal)
        
        inputView.addSubview(cancelButton) // add Button to UIView
        
        cancelButton.addTarget(self, action: #selector(BookTable.cancelButton(_:)), for: UIControlEvents.touchUpInside) // set button click event
        
        sender.inputView = inputView
        datePickerView.addTarget(self, action: #selector(BookTable.handleDatePicker(_:)), for: UIControlEvents.valueChanged)
        
        handleDatePicker(datePickerView) // Set the date on start.
    }
   
    
    override func viewDidLoad() {
        //<#code#>
//        dateSet.userInteractionEnabled = true
//        let aSelector : Selector = "lblTapped"
//        let tapGesture = UITapGestureRecognizer(target: self, action: aSelector)
//        tapGesture.numberOfTapsRequired = 1
//        
//        dateSet.addGestureRecognizer(tapGesture)
//        textF.text = "Date"
//        textF.textColor = UIColor.blackColor()

    }
    
    func handleDatePicker(_ sender: UIDatePicker) {
        print("Date set")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-YYYY"
        dateText.text = dateFormatter.string(from: sender.date)
    }
    
    func doneButton(_ sender:UIButton)
    {
        if sender.tag == 12
        {
            dateText.resignFirstResponder() // To resign the inputView on clicking done.
        }
        else
        {
            timeText.resignFirstResponder() // To resign the inputView for time on clicking done.
        }
    }
    
    func cancelButton(_ sender:UIButton)
    {
        if sender.tag == 14
        {
            dateText.resignFirstResponder() // To resign the inputView on clicking done.
        }
        else
        {
            timeText.resignFirstResponder() // To resign the inputView for time on clicking done.
        }
    }
    
    
    @IBAction func timeSetter(_ sender: UITextField) {
        print("timeaction")
     
        //Create the view
        let inputView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 240))
        
        let inputButtonView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 40))
        inputButtonView.backgroundColor = UIColor.gray
        inputView.addSubview(inputButtonView) // add UIView inside UIView
        
        let timePickerView  : UIDatePicker = UIDatePicker(frame: CGRect(x: (self.view.frame.width/3) - 100, y: 40, width: 0, height: 0))
        timePickerView.datePickerMode = UIDatePickerMode.time
        
        inputView.addSubview(timePickerView) // add time picker to UIView
        
        print("--time--\(self.view.frame.size.width)")
        
        //        let image1 = UIImage(named: "foodicon.png")! as UIImage
        //        let image2 = UIImage(named: "halal.png")! as UIImage
        
        // done button-----------
        let doneButton = UIButton(frame: CGRect(x: 10, y: 0, width: 50, height: 40))
        
        
        doneButton.setTitle("Done", for: UIControlState())
        doneButton.setTitle("Done", for: UIControlState.highlighted)
        doneButton.setTitleColor(UIColor.black, for: UIControlState())
        doneButton.setTitleColor(UIColor.gray, for: UIControlState.highlighted)
        doneButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        doneButton.tag = 15
        //        doneButton.setBackgroundImage(image1, forState: UIControlState.Normal)
        
        inputView.addSubview(doneButton) // add Button to UIView
        
        doneButton.addTarget(self, action: #selector(BookTable.doneButton(_:)), for: UIControlEvents.touchUpInside) // set button click event
        
        let titleLable = UILabel(frame: CGRect( x: (self.view.frame.width/2) - ((100/2)-20), y: 0,width: 60, height: 40 ) )
        titleLable.text = "Time"
        titleLable.textColor = UIColor.white
        titleLable.font = UIFont.boldSystemFont(ofSize: 20.0)
        titleLable.tag = 16
        
        inputView.addSubview(titleLable) // add Label to UIView
        
        // cancel-----------
        let cancelButton = UIButton(frame: CGRect(x: self.view.frame.width - 80, y: 0,width: 70, height: 40))
        cancelButton.setTitle("Cancel", for: UIControlState())
        cancelButton.setTitle("Cancel", for: UIControlState.highlighted)
        cancelButton.setTitleColor(UIColor.black, for: UIControlState())
        cancelButton.setTitleColor(UIColor.gray, for: UIControlState.highlighted)
        cancelButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        cancelButton.tag = 17
        //        cancelButton.setBackgroundImage(image2, forState: UIControlState.Normal)
        
        inputView.addSubview(cancelButton) // add Button to UIView
        
        cancelButton.addTarget(self, action: #selector(BookTable.cancelButton(_:)), for: UIControlEvents.touchUpInside) // set button click event
        
        sender.inputView = inputView
        timePickerView.addTarget(self, action: #selector(BookTable.handleTimePicker(_:)), for: UIControlEvents.valueChanged)
        
        handleTimePicker(timePickerView) // Set the date on start.
    }
    
    
    
//    func lblTapped(){
//        dateSet.hidden = true
//        textF.hidden = false
//        textF.text = "Hai"
//    }
    
    
   
    
    
    func handleTimePicker(_ sender: UIDatePicker) {
         let timeFormatter = DateFormatter()
          timeFormatter.dateFormat = "HH:mm"
          timeText.text = timeFormatter.string(from: sender.date)
    }
    
    
    
//    func textFieldShouldReturn(userText: UITextField) -> Bool {
//        userText.resignFirstResponder()
//        textF.hidden = true
//        dateSet.hidden = false
//        dateSet.text = textF.text
//        return true
//    }
    
//    func lblTapped(sender: UITextField){

//    }
//    

}
