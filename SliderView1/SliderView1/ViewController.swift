//
//  ViewController.swift
//  SliderView1
//
//  Created by FiveExceptions1 on 16/09/16.
//  Copyright © 2016 FiveExceptions1. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet var slide1: UISlider!
    
    @IBOutlet var label1: UILabel!
    
    
    @IBOutlet var label2: UILabel!
    @IBOutlet var segment1: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func slideFunc1(sender: UISlider) {
        print("jio")
        var currentValue = Int(sender.value)
        
        label1.text = "\(currentValue)"
    }
    
    
    @IBAction func segmentFunc1(sender: AnyObject) {
        
        switch segment1.selectedSegmentIndex
        {
        case 0:
            label2.text = "First selected";
        case 1:
            label2.text = "Second Segment selected";
        default:
            break; 
        }
        
    }
    
}

