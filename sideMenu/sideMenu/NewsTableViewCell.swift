//
//  NewsTableViewCell.swift
//  sideMenu
//
//  Created by FiveExceptions1 on 12/09/16.
//  Copyright © 2016 FiveExceptions1. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {
    
//    @IBOutlet weak var postImageView:UIImageView!
//    @IBOutlet weak var authorImageView:UIImageView!
//    @IBOutlet weak var postTitleLabel:UILabel!
//    @IBOutlet weak var authorLabel:UILabel!
    
    
    @IBOutlet var postImageView: UIImageView!
    
    @IBOutlet var postTitleLabel: UILabel!
    
    @IBOutlet var authorImageView: UIImageView!
    
    @IBOutlet var authorLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
