// This code accompanies the following post: http://sketchytech.blogspot.co.uk/2014/11/swift-translating-and-rotating.html

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let newView = View(frame: CGRect(x: 0, y: 0, width: CGRectGetWidth(self.view.frame), height: CGRectGetHeight(self.view.frame)))
        
        self.view.addSubview(newView)
        
        
    }
    
}

class View:UIView {
//    override func drawRect(rect:CGRect)        
//    {
//        // obtain context
//        let ctx = UIGraphicsGetCurrentContext()
//        
//        // Decide on radius
//        let rad = CGRectGetWidth(rect)/3.5
//        // End angle will be 2*pi for any circle that begins at 0
//        let endAngle = CGFloat(2*M_PI)
//        
//        // We could use CGContextAddEllipseInRect to draw a circle instead
//        CGContextAddArc(ctx!, CGRectGetMidX(rect), CGRectGetMidY(rect), rad, 0, endAngle, 1)
//        CGContextAddArc(ctx!, CGRectGetMidX(rect), CGRectGetMidY(rect), rad, 0, endAngle, 1)
//
//        // set stroke color
//        CGContextSetStrokeColorWithColor(ctx!,UIColor.whiteColor().CGColor)
//        // Set line width
//        CGContextSetLineWidth(ctx!, 4.0)
//        CGContextStrokePath(ctx!)
//        
//        
//        
//        // save current state of context
//        CGContextSaveGState(ctx!)
//        
//        CGContextTranslateCTM(ctx!, CGRectGetMidX(rect), CGRectGetMidY(rect))
//        // rotation supplied in radians
//
//        
//        //for i in 0...100 {
//            //var j = Double(i)
//            //print(j)
////            CGContextRotateCTM(ctx!, CGFloat(M_PI*10/180))
//        
//            CGContextRotateCTM(ctx!, CGFloat(M_PI*(270)/180))
//            //print("----------------\(CGFloat(M_PI*10/180))" )
//        //}
//        
//        // create a mutable path
//        let path = CGPathCreateMutable()
//        // move to the starting point of the path
//        CGPathMoveToPoint(path, nil, 0, 0)
//        // add a line the length of the radius to path
//        CGPathAddLineToPoint(path, nil, rad, 1)
//        // add the path to the (translated and rotated) context
//        CGContextAddPath(ctx!, path)
//        CGContextSetTextMatrix(ctx!, CGAffineTransformMakeScale(1.0, -1.0))
//        // set line width
//        CGContextSetLineWidth(ctx!, 10)
//        // set line color
//        CGContextSetStrokeColorWithColor(ctx!,UIColor.greenColor().CGColor)
//        // stroke path
//        CGContextStrokePath(ctx!)
//        
//        // restore original state
//        //CGContextRestoreGState(ctx!)
//        
//    }
    
    override func drawRect(rect:CGRect)  {
        let path = CGPathCreateMutable()
        
        CGPathMoveToPoint (path, nil, points[0].x, points[0].y)
        
        for i in 1 ..< points.count {
            CGPathAddLineToPoint (path, nil, points[i].x, points[i].y)
        }
        
        CGContextAddPath (context, path)
        CGContextStrokePath (context)
    }
    
    
}
