import UIKit

class ShapeView: UIView {
    var currentShapeType: Int = 0
    var lineS:CGPoint = CGPointMake(30, 70)
    var lineE:CGPoint = CGPointMake(210, 70)
    var touchDown:CGPoint = CGPointZero
    var endPoint:CGPoint = CGPointZero
    
    init(frame: CGRect, shape: Int) {
        super.init(frame: frame)
        self.currentShapeType = shape
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func drawRect(rect: CGRect) {
        print("de value")
        switch currentShapeType {
        case 0: drawLines()
        case 1: drawLines()
        //case 2: drawCircle()
        default: print("default")
        }
        
        
    }
    
    func drawLines() {
        //1
        print("draw line")
        let ctx = UIGraphicsGetCurrentContext()
        
        //2
        CGContextBeginPath(ctx!)
        CGContextSetLineWidth(ctx!, 5)
        CGContextSetStrokeColorWithColor(ctx!,UIColor.blackColor().CGColor)
        CGContextMoveToPoint(ctx!, lineS.x, lineS.y)
        CGContextAddLineToPoint(ctx!,lineE.x, lineE.y)
//
//        CGContextClosePath(ctx!)
//        CGContextMoveToPoint(ctx!, 55.0, 105.0)
//        CGContextAddLineToPoint(ctx!, 80.0, 80.0)
//        CGContextAddLineToPoint(ctx!, 100.00, 200.0)
        
        
        
        
        //ctx?.setLineWidth(5)
        
        //3
        //ctx?.closePath()
        CGContextStrokePath(ctx!)
        
//        draw1()

    }
//    
//    func draw1(){
//        let ctx1 = UIGraphicsGetCurrentContext()
//        
//        //2
//        CGContextBeginPath(ctx1!)
//        CGContextSetLineWidth(ctx1!, 5)
//        CGContextSetStrokeColorWithColor(ctx1!,UIColor.blackColor().CGColor)
//        CGContextMoveToPoint(ctx1!, 200.0, 120.0)
//        CGContextAddLineToPoint(ctx1!, 10.0, 40.0)
//        
//        CGContextAddLineToPoint(ctx1!, 50.0, 10.0)
//        CGContextAddLineToPoint(ctx1!, 10.00, 20.0)
//        
//        
//        
//        
//        //ctx?.setLineWidth(5)
//        
//        //3
//        //ctx?.closePath()
//        CGContextStrokePath(ctx1!)
//    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let lastPoint = touches.first?.locationInView(self)
        touchDown = (touches.first?.locationInView(self))!
        print("last\(lastPoint)")
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let lastPoint = touches.first?.locationInView(self)
        
        lineE = lastPoint!
        lineS = lastPoint!
        print("last\(lastPoint)")
        
        if let touch = touches.first {
            endPoint = touch.locationInView(self)
            let angle = self.angle(touchDown, end: endPoint)
            print("the angle is \(angle)")
        }
        
        self.setNeedsDisplay()
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let lastPoint = touches.first?.locationInView(self)
        lineE = lastPoint!
        lineS = lastPoint!
        print("last\(lastPoint)")
        self.setNeedsDisplay()
    }

    func angle(start:CGPoint,end:CGPoint)->Double {
        
        let dx = end.x - start.x
        let dy = end.y - start.y
        let abs_dy = fabs(dy);
        
        //calculate radians
        let theta = atan(abs_dy/dx)
        let mmmm_pie:CGFloat = 3.1415927
        
        //calculate to degrees , some API use degrees , some use radians
        let degrees = (theta * 360/(2*mmmm_pie)) + (dx < 0 ? 180:0)
        
        //transmogrify to negative for upside down angles
        let negafied = dy > 0 ? degrees * -1:degrees
        
        return Double(negafied)
        
    }
    
    
}
