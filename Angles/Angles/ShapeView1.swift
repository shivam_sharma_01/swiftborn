import UIKit

extension Double {
    /// Rounds the double to decimal places value
    func roundToPlaces(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return round(self * divisor) / divisor
    }
}

class ShapeView1:UIView {
    
//    var touchDown:CGPoint = CGPointZero
    var touchDown:CGPoint = CGPointMake(30, 70)
    var endPoint:CGPoint = CGPointZero
    
    var touchDown1:CGPoint = CGPointMake(60, 140)
    var endPoint1:CGPoint = CGPointMake(60, 240)
    
    var gotLock:Bool = false
    
    var gotLock1:Bool = false
    var gotLock2:Bool = false
    var gotLock3:Bool = false
    var gotLock4:Bool = false
    
    
    
    var currentShapeType: Int = 0
   
  
    
    init(frame: CGRect, shape: Int) {
        super.init(frame: frame)
        self.currentShapeType = shape
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {        
        //we want only one pointy thing
//        print("touches.count ---\(touches.count)")
        var near = (touches.first?.locationInView(self))!
        var length1 = Int(sqrt(  ( pow( (near.x)-(touchDown.x), 2 ) + pow( (near.y)-(touchDown.y),2) ) ))
        var length2 = Int(sqrt(  ( pow( (near.x)-(endPoint.x), 2 ) + pow( (near.y)-(endPoint.y),2) ) ))
        
        var length3 = Int(sqrt(  ( pow( (near.x)-(touchDown1.x), 2 ) + pow( (near.y)-(touchDown1.y),2) ) ))
        var length4 = Int(sqrt(  ( pow( (near.x)-(endPoint1.x), 2 ) + pow( (near.y)-(endPoint1.y),2) ) ))

        print("BEGAN ---")
//        if touches.count == 1 {
            gotLock = true
//            if let touch = touches.first! as? UITouch {
//                touchDown = touch.locationInView(self)
//                touchDown = (touches.first?.locationInView(self))!
//                endPoint = (touches.first?.locationInView(self))!
//            }
//        }
        
        print("length1 -- \(length1)---length1---\(length1)")
        
        if (length1 < length2) && (length1 < length3) && (length1 < length4) {
            touchDown = (touches.first?.locationInView(self))!
            gotLock1 = true
            gotLock2 = false
            gotLock3 = false
            gotLock4 = false
            
            print("start--1")
        }
        else if (length1 > length2) && (length3 > length2) && (length4 > length2) {
            endPoint = (touches.first?.locationInView(self))!
            gotLock1 = false
            gotLock2 = true
            gotLock3 = false
            gotLock4 = false
            print("start--2")
        }
        else if (length3 < length1) && (length3 < length2) && (length3 < length4) {
            touchDown1 = (touches.first?.locationInView(self))!
            gotLock1 = false
            gotLock2 = false
            gotLock3 = true
            gotLock4 = false
            print("start--3")
        }
        else if (length1 > length4) && (length2 > length4) && (length3 > length4) {
            endPoint1 = (touches.first?.locationInView(self))!
            gotLock1 = false
            gotLock2 = false
            gotLock3 = false
            gotLock4 = true
            print("start--4")
        }
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        print("MOVED ---")
        
        var near = (touches.first?.locationInView(self))!
        var length1 = Int(sqrt(  ( pow( (near.x)-(touchDown.x), 2 ) + pow( (near.y)-(touchDown.y),2) ) ))
        var length2 = Int(sqrt(  ( pow( (near.x)-(endPoint.x), 2 ) + pow( (near.y)-(endPoint.y),2) ) ))
        
        var length3 = Int(sqrt(  ( pow( (near.x)-(touchDown1.x), 2 ) + pow( (near.y)-(touchDown1.y),2) ) ))
        var length4 = Int(sqrt(  ( pow( (near.x)-(endPoint1.x), 2 ) + pow( (near.y)-(endPoint1.y),2) ) ))
        
//        if touches.count == 1 {
//            if let touch = touches.first! as? UITouch {
//                endPoint = touch.locationInView(self)
//                endPoint = (touches.first?.locationInView(self))!
                let angle = self.angle(touchDown, end: endPoint)
//                print("the angle is \(angle)")
        
                
//            }
//        }
//        else {
//            gotLock = false
//        }
                                                                        // true false if one line is holded working
        if (length1 < length2) && (length1 < length3) && (length1 < length4) && gotLock1 {
            touchDown = (touches.first?.locationInView(self))!
            gotLock1 = true
            gotLock2 = false
            gotLock3 = false
            gotLock4 = false
            print("start--1")
        }
        else if (length1 > length2) && (length3 > length2) && (length4 > length2) && gotLock2{
            endPoint = (touches.first?.locationInView(self))!
            gotLock1 = false
            gotLock2 = true
            gotLock3 = false
            gotLock4 = false
            print("start--2")
        }
        else if (length3 < length1) && (length3 < length2) && (length3 < length4) && gotLock3 {
            touchDown1 = (touches.first?.locationInView(self))!
            gotLock1 = false
            gotLock2 = false
            gotLock3 = true
            gotLock4 = false
            print("start--3")
        }
        else if (length1 > length4) && (length2 > length4) && (length3 > length4) && gotLock4 {
            endPoint1 = (touches.first?.locationInView(self))!
            gotLock1 = false
            gotLock2 = false
            gotLock3 = false
            gotLock4 = true
            print("start--4")
        }
        
        self.setNeedsDisplay()
        
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        print("ENDED ---")
        
        var near = (touches.first?.locationInView(self))!
        var length1 = Int(sqrt(  ( pow( (near.x)-(touchDown.x), 2 ) + pow( (near.y)-(touchDown.y),2) ) ))
        var length2 = Int(sqrt(  ( pow( (near.x)-(endPoint.x), 2 ) + pow( (near.y)-(endPoint.y),2) ) ))
        
        var length3 = Int(sqrt(  ( pow( (near.x)-(touchDown1.x), 2 ) + pow( (near.y)-(touchDown1.y),2) ) ))
        var length4 = Int(sqrt(  ( pow( (near.x)-(endPoint1.x), 2 ) + pow( (near.y)-(endPoint1.y),2) ) ))
//                if touches.count == 1 {
//                    if let touch = touches.first! as? UITouch {
//                        endPoint = touch.locationInView(self)
            //main
//                          endPoint = (touches.first?.locationInView(self))!
//                        let angle = self.angle(touchDown, end: endPoint)
//                        print("the angle is \(angle)")
        
        
//                    }
//                }
//                else {
//                    gotLock = false
//                }
        
        if (length1 < length2) && (length1 < length3) && (length1 < length4) {
            touchDown = (touches.first?.locationInView(self))!
            gotLock1 = false
            gotLock2 = false
            gotLock3 = false
            gotLock4 = false
            print("start--1")
        }
        else if (length1 > length2) && (length3 > length2) && (length4 > length2) {
            endPoint = (touches.first?.locationInView(self))!
            gotLock1 = false
            gotLock2 = false
            gotLock3 = false
            gotLock4 = false
            print("start--2")
        }
        else if (length3 < length1) && (length3 < length2) && (length3 < length4) {
            touchDown1 = (touches.first?.locationInView(self))!
            gotLock1 = false
            gotLock2 = false
            gotLock3 = false
            gotLock4 = false
            print("start--3")
        }
        else if (length1 > length4) && (length2 > length4) && (length3 > length4) {
            endPoint1 = (touches.first?.locationInView(self))!
            gotLock1 = false
            gotLock2 = false
            gotLock3 = false
            gotLock4 = false
            print("start--4")
        }
        self.setNeedsDisplay()
        
    }
    
    func angle(start:CGPoint,end:CGPoint)->Double {
        
        let dx = end.x - start.x
        let dy = end.y - start.y
        let abs_dy = fabs(dy);
        
        //calculate radians
        let theta = atan(abs_dy/dx)
        let mmmm_pie:CGFloat = 3.1415927
        
        //calculate to degrees , some API use degrees , some use radians
        let degrees = (theta * 360/(2*mmmm_pie)) + (dx < 0 ? 180:0)
        
        //transmogrify to negative for upside down angles
        let negafied = dy > 0 ? degrees * -1:degrees
        
        return Double(negafied)
        
    }
    
    override func drawRect(rect: CGRect) {
        
//        if gotLock  {
        
//            var level  = UIBezierPath()
//            level.moveToPoint(CGPointMake(0, touchDown.y))
//            level.addLineToPoint(CGPointMake(CGRectGetWidth(self.frame), touchDown.y))
//            level.stroke()
            
            print("touch--\(touchDown)----endPoint----\(endPoint)")
        
        
        
        
            var path = UIBezierPath()
            path.moveToPoint(touchDown)
            path.addLineToPoint(endPoint)
            
            let ctx = UIGraphicsGetCurrentContext()
            let aFont = UIFont(name: "Arial", size: 15)
            // create a dictionary of attributes to be applied to the string
            let attr:CFDictionaryRef = [NSFontAttributeName:aFont!,NSForegroundColorAttributeName:UIColor.blackColor()]
            // create the attributed string
            

            var angle = self.angle(touchDown, end: endPoint).roundToPlaces(0)
            if (angle >= 0)
            {
                
                // do positive stuff
            }
            else
            {
                angle = 360.00 + (angle)
                // do negative stuff
            }
            
     
            //var length = touchDown.x - endPoint.x
            var length = Int(sqrt(  ( pow( (endPoint.x)-(touchDown.x), 2 ) + pow( (endPoint.y)-(touchDown.y),2) ) ))
            
            var text_desc = "\(angle.description),\(length)"
            
            print("length---\(length)")
            let text1 = CFAttributedStringCreate(nil, text_desc, attr)
      
            // create the line of text
            let line1 = CTLineCreateWithAttributedString(text1)
  
            // retrieve the bounds of the text
            
            // set the line width to stroke the text with
            CGContextSetLineWidth(ctx!, 1.5)
            // set the drawing mode to stroke
            CGContextSetTextDrawingMode(ctx!, .Fill)
            
            CGContextSetTextMatrix(ctx!, CGAffineTransformMakeScale(1.0, -1.0))
            CGContextSetTextPosition(ctx!, touchDown.x + 20, touchDown.y + 40)
            CTLineDraw(line1, ctx!)
            
            path.stroke()
        
        
            var path1 = UIBezierPath()
            path1.moveToPoint(touchDown1)
            path1.addLineToPoint(endPoint1)
        
            path1.stroke()
        
//        }
    }
    
}
