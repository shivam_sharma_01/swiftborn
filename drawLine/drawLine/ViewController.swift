//
//  ViewController.swift
//  drawLine
//
//  Created by FiveExceptions1 on 13/10/16.
//  Copyright © 2016 FiveExceptions1. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let tapGR = UITapGestureRecognizer(target: self, action: #selector(ViewController.didTap(_:)))
        
        self.view.addGestureRecognizer(tapGR)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didTap(tapGR: UITapGestureRecognizer) {
        
        let tapPoint = tapGR.locationInView(self.view)
        print("view controller--\(tapPoint)")
        let shapeView = ShapeView(origin: tapPoint)
        
        self.view.addSubview(shapeView)
    }

}

