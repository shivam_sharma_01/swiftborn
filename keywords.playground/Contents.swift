//: Playground - noun: a place where people can play

import UIKit
import Foundation


func greet(person: [String: String]) {
    guard let name = person["name"] else {
        print("else")
        return
    }
    
    print("Hello \(name)!")
    
    guard let location = person["location"] else {
        print("I hope the weather is nice near you.")
        return
    }
    
    print("I hope the weather is nice in \(location).")
}

var hai = greet(person: ["name": ""])
print("hai \(hai)")
// Prints "Hello John!"
// Prints "I hope the weather is nice near you."
greet(person: ["name": "Jane", "location": "Cupertino"])
// Prints "Hello Jane!"
// Prints "I hope the weather is nice in Cupertino."

