//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

extension Int {
    var degreesToRadians: Double { return Double(self) * (22/7) / 180 }
    var radiansToDegrees: Double { return Double(self) * 180 / (22/7) }
}
extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}

270.degreesToRadians            // 0.7853981633974483
//Double(45).degreesToRadians    // 0.7853981633974483
//CGFloat(45).degreesToRadians   // 0.785398163397448
//Float(45).degreesToRadians     // 0.7853982
//Float80(45).degreesToRadians   // 0.785398163397448278999
