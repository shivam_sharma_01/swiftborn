//
//  ViewController.swift
//  touchEx
//
//  Created by FiveExceptions1 on 18/10/16.
//  Copyright © 2016 FiveExceptions1. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.view.userInteractionEnabled = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        
//        let touch: UITouch = (touches.first as UITouch?)!
//        
        
        if let touch = touches.first as UITouch? {
            let location = touch.locationInView(self.imageView)
            print("Location: \(location)")
            self.imageView.transform = CGAffineTransformMakeTranslation(location.x, location.y)
            print("---------------\(self.imageView.transform)")
            
        }
//        if (touch.view == imageView){
//            print("touchesBegan | This is an ImageView")
//        }else{
//            print("touchesBegan | This is not an ImageView")
//        }
    }
    
    
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesMoved(touches, withEvent: event)
        
        
//        let touch: UITouch = (touches.first as UITouch?)!
//                if (touch.view == imageView){
//            print("touchesMoved | This is an ImageView")
//        }else{
//            print("touchesMoved | This is not an ImageView")
//        }

    }
    
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesMoved(touches, withEvent: event)
        
//        let touch: UITouch = (touches.first as UITouch?)!
        
       

    }
   
}


class Shape : UIView{
    func draw(rect: CGRect) {
        print("hai")
        let ctx = UIGraphicsGetCurrentContext();
        let path = CGPathCreateMutable();
        CGPathMoveToPoint(path, nil, 0, 0);
        CGPathAddLineToPoint(path, nil, CGRectGetMaxX(rect), CGRectGetMaxY(rect));
        CGPathCloseSubpath(path);
        CGContextAddPath(ctx!, path);
        CGContextSetStrokeColorWithColor(ctx!,UIColor.greenColor().CGColor);
        CGContextStrokePath(ctx!);
//        CGPathRelease(path);
    }
}
