//
//  TableView.swift
//  pageController
//
//  Created by FiveExceptions1 on 16/09/16.
//  Copyright © 2016 FiveExceptions1. All rights reserved.
//

import UIKit

class TableViewPageController: UIPageViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataSource = self
        
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                direction: .Forward,
                animated: true,
                completion: nil)
        }
    }
    
    // MARK: UIPageViewControllerDataSource
    
    extension TutorialPageViewController: UIPageViewControllerDataSource {
        
        func pageViewController(pageViewController: UIPageViewController,
            viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
                return nil
        }
        
        func pageViewController(pageViewController: UIPageViewController,
            viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
                return nil
        }
        
    }
    private(set) lazy var orderedViewControllers: [UIViewController] = {
        return [self.newColoredViewController("Green"),
            self.newColoredViewController("Red"),
            self.newColoredViewController("Blue")]
    }()
    
    private func newColoredViewController(color: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil) .
            instantiateViewControllerWithIdentifier("\(color)ViewController")
    }
}
