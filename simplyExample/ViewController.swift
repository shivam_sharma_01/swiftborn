//
//  ViewController.swift
//  simplyExample
//
//  Created by FiveExceptions1 on 20/09/16.
//  Copyright © 2016 FiveExceptions1. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {
    
    @IBOutlet var myTable: UITableView!
    let cellSpacingHeight: CGFloat = 5
    var locations = ["asian","banting","breakfast","burger","child friendly","cookery classes","coffee",
    "dersert","drink","indian","italian","luxury dining","meat lover","mexican","nightlife","pet friendly","recipes","seafood","streetfood","sushi","vegetarian","vegan"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    //        return 5
    //    }
    
    override func tableView(tableView: UITableView,
        numberOfRowsInSection section: Int) -> Int {
            return locations.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 100 //cell height
        //        if indexPath.row % 2 == 0
        //        {
        //            return 100 //cell height
        //        }
        //        else
        //        {
        //            return 5 //space heigh
        //        }
    }
    
    override func tableView(tableView: UITableView,
        willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
            return nil
    }
    
    override func tableView(tableView: UITableView,
        cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
            
            let cell:CustomCell = self.myTable.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! CustomCell
            
          
            
            cell.categories.backgroundColor = UIColor.redColor()
            
            cell.categories.text = " "+(locations[indexPath.row])
            return cell
    }
    
    //    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    //        return 140.0
    //    }
    
    //    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //        return 40
    //    }
    
    
}

