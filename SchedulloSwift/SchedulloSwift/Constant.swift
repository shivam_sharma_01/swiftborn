//
//  Constant.swift
//  SchedulloSwift
//
//  Created by FiveExceptions1 on 06/02/17.
//  Copyright © 2017 FiveExceptions1. All rights reserved.
//

import Foundation


class Constant
{
    
    //MARK: Constants
    static let sharedInstance = Constant()
    
    
    
    
    static let BASE_URL = "https://test.schedullo.com/index.php/APIS/api/";
    // let SERVER_URL = "https://dev.schedullo.com/index.php/APIS/api/";
    // let SERVER_URL = "http://192.168.0.2/schedullo_API/index.php/APIS/api/";
    // let SERVER_URL = "http://192.168.0.18/schedullo_A/index.php/APIS/api/";
    // let SERVER_URL = "http://192.168.0.2/schedullo/index.php/APIS/api/";
    //  let SERVER_URL = "http://192.168.0.3/schedullo/index.php/APIS/api/";
    
//    static let BASE_URL = "http://test.crowdandco.com.au/startingbrief/apis/"
    
//    static let BASE_URL = "http://192.168.0.9/starting_brief/question2answer/apis/"
    
//    static let ALL_QUESTION = 1
//    
//    static let UNANSWERED_QUESTION = 2
//    
//    static let MY_QUESTION = 4
//    
//    static let CROWD_BRIEF = 5
//    
//    static let MY_BLOG = 6
//    
//    static let POST_ANSWER = 10
//    
//    static let BLOG_COMMENT = 20
//    
//    static let COMMENT_ENABLE = true
//    
//    static let MAX_TAG_MSG = "Only 4 tags allow"
//    
//    static let APP_COLOR = "#00B3D3"
//    
//    static let REG_EXP = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
    
}
