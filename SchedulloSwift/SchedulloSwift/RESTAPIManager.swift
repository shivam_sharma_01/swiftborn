//
//  RESTAPIManager.swift
//  SchedulloSwift
//
//  Created by FiveExceptions1 on 14/02/17.
//  Copyright © 2017 FiveExceptions1. All rights reserved.
//
import UIKit

import CommonCrypto

//MARK: TypeAlias
typealias ServiceResponse = (JSON, NSError?) -> Void

class RESTAPIManager: NSObject {
    
    //MARK: SharedInstance
    static let restAPIManager = RESTAPIManager()
    
    //MARK: Perform POST Request
    func makeHTTPPOSTRequest(path: String, body: [String: AnyObject],onCompletion: ServiceResponse) {
        
        let request = NSMutableURLRequest(URL: NSURL(string: path)!)
        
        let Username = "test"
        
        let Password = "CC1981#"
        
        let loginString = String(format: "%@:%@", Username, Password)
        
        let loginData = loginString.dataUsingEncoding(NSUTF8StringEncoding)
        
        let base64LoginString = loginData?.base64EncodedDataWithOptions([])
        
        let datastring1 = NSString(data: base64LoginString!, encoding: NSUTF8StringEncoding)!
        
        print("datastring1--\(datastring1)")
        /* let headers = [
         "authorization": "Basic \(datastring1!)",
         "content-type": "application/x-www-form-urlencoded",
         "cache-control": "no-cache"]*/
        
        
        // "postman-token": "caaab871-3c21-60f8-e66d-594216e78740"
        
        request.HTTPMethod = "POST"
        // request.setValue("Basic \(datastring1)", forHTTPHeaderField: "authorization")
        //request.allHTTPHeaderFields = headers
        
        do {
            
            // Set the POST body for the request
            let jsonBody = try NSJSONSerialization.dataWithJSONObject(body, options: [])
            
            let datastring = NSString(data: jsonBody, encoding: NSUTF8StringEncoding)
            
            print("datastring info --\(datastring!)")
            
            
            
            let newString:String = datastring!.stringByReplacingOccurrencesOfString(" ", withString: "")
            
            let stringWithoutSpace:String = newString.stringByReplacingOccurrencesOfString("\n", withString: "")
            
//            let hmacResult:String = "\(datastring!)".digest(HMACAlgorithm.SHA1, key: "blu3@T0pcash-C0wss5X")!
//            print("onemac--\(hmacResult)")
            
            let hmacResult:String = "\(stringWithoutSpace)".digest(HMACAlgorithm.SHA1, key: "blu3@T0pcash-C0wss5X")
            
            //let hmacResult:String = "\(stringWithoutSpace)".digest(HMACAlgorithm.SHA1, key: "pr_1407_ke_1409")
            
            // print(jsonBody)
            //print(stringWithoutSpace)
            
            print("\(hmacResult)******")
            
            //let headers = ["authorization": "Basic \(datastring1!)","salt":"\(hmacResult)","content-type": "application/x-www-form-urlencoded","cache-control": "no-cache"]
            
            let headers = ["content-type": "application/json","salt":"\(hmacResult)"]
            
            request.allHTTPHeaderFields = headers
            
            for h in request.allHTTPHeaderFields!
            {
                
                print("\(h.1)---")
                
            }
            
            request.HTTPBody = jsonBody
            
            let session = NSURLSession.sharedSession()
            
            let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
                
                print("Response ***** \(response)")
                
                if let jsonData = data {
                    
                    let json:JSON = JSON(data: jsonData)
                    
                    onCompletion(json, nil)
                    
                } else {
                    
                    onCompletion(nil, error)
                    
                }
                
            })
            
            task.resume()
            
        } catch {
            
            // Create your personal error
            onCompletion(nil, nil)
            
        }
    }
}
