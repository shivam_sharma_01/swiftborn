//
//  TaskInfo.swift
//  SchedulloSwift
//
//  Created by FiveExceptions1 on 06/02/17.
//  Copyright © 2017 FiveExceptions1. All rights reserved.
//

import Foundation

class TaskInfo {
    static let sharedUserDetail = TaskInfo()
    var task_id : String = ""
    var user_id : String = ""
    var company_id : String = ""
    var task_name : String = ""
    var task_descriptions : String = ""
    var task_due_date : String = ""
    var task_schedulle_date : String = ""
    var task_status_id : String = ""
    var task_status : String = ""
    var task_priority_id : String = ""
    var task_user_id : String = ""
    var task_users : String = ""
    var task_project_id : String = ""
    var task_project : String = ""
    var task_isWatch : String = ""
    var task_isPrivate : String = ""
    var task_estimate_time : String = ""
    var task_actual_time : String = ""
    var task_comment_id : String = ""
    var task_comments : String = ""
    
    var sorting_id : String = ""
    
    var color_id : String = ""
    var color_name : String = ""

    var priority_id : String = ""
    var priority_name: String = ""
    
    // add task
    func addTask(){
    
    }
    
    // edit task
    func editTask(){
        
    }
    
    // delete task
    func deleteTask(){
        
    }
    
    // add comments
    func addComments(){
        
    }
    
    // get comments
    func getComments(){
        
    }
    
    // get status
    func getStatus(){
        
    }
    
    // get priority
    func getPriority(){
        
    }
    
    // get sortings
    func getSorting(){
        
    }
    
    // get colors
    func getColors(){
        
    }
    
    // get status based task
    func getStatusBasedTask(){
        
    }
    
    // get week for all and open task
    func getWeeks(){
        
    }
    
    // show Date format
    func showDateInFormat(){
        
    }
    
    // get Time Format like 0:00
    func getTimeFormat(){
        
    }
    
    // get total estimated time
    func getTotalEstimatedTime(){
        
    }
}
