//
//  LoginInfo.swift
//  SchedulloSwift
//
//  Created by FiveExceptions1 on 06/02/17.
//  Copyright © 2017 FiveExceptions1. All rights reserved.
//

import Foundation

class LoginInfo {
    
    
    
   // var wingLength: String = ""
    
    var email_id_value: String = ""
    var password_value: String = ""
    var user_id_value: String = ""
    var token_value: String = ""
   // var companyInfo = [String: String]()
    var companyName:String!
    var token:String!
    var lastName:String!
    var userID:String!
    var companyID:String!
    var firstName:String!
    var compnyList:[JSON] = [JSON]()

    init(json:JSON)
    {
        
        self.companyName = json["company_name"].string
        
        self.token  = json["token"].string
        
        self.lastName = json["last_name"].string
        
        self.userID = json["user_id"].string
        
        self.companyID = json["company_id"].string
        
        self.firstName = json["first_name"].string
    }
    
    /*var wingSpan: String {
        get {
            // This computed property is based on wingLength.
            return wingLength + " and carriage"
        }
        set {
            // Store the results of a computation.
            wingLength = newValue
        }
    }*/
    
    //email
    var email : String {
        get {
            // This computed property is based on wingLength.
            return email_id_value
        }
        set {
            // Store the results of a computation.
            email_id_value = newValue
        }
    }
    
    // password
    var password : String {
        get {
            // This computed property is based on wingLength.
            return password_value
        }
        set {
            // Store the results of a computation.
            password_value = newValue
        }
    }
    
    // user id
    var user_id : String {
        get {
            // This computed property is based on wingLength.
            return user_id_value
        }
        set {
            // Store the results of a computation.
            user_id_value = newValue
        }
    }

    /*var token : String {
        get {
            // This computed property is based on wingLength.
            return token_value
        }
        set {
            // Store the results of a computation.
            token_value = newValue
        }
    }*/

    
    
//
//    
//    
//    // company info
//    func setCompaniesInfo(){
//        
//    }
//    
//    func getCompaniesInfo(){
//        

}
