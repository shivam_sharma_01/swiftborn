//
//  MapViewController.swift
//  SchedulloSwift
//
//  Created by FiveExceptions1 on 28/02/17.
//  Copyright © 2017 FiveExceptions1. All rights reserved.
//

import UIKit


class MapViewController: UIViewController {
    
    
    @IBOutlet var menuButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if revealViewController() != nil {
            menuButton.target = revealViewController()
            menuButton.action = "revealToggle:"
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //
    //
    //    /*
    //    // MARK: - Navigation
    //
    //    // In a storyboard-based application, you will often want to do a little preparation before navigation
    //    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    //    // Get the new view controller using segue.destinationViewController.
    //    // Pass the selected object to the new view controller.
    //    }
    //    */
    //
}
