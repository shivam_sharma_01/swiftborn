//
//  MenuController.swift
//  SchedulloSwift
//
//  Created by FiveExceptions1 on 28/02/17.
//  Copyright © 2017 FiveExceptions1. All rights reserved.
//

import UIKit

class MenuController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("Row number ---\(indexPath.row)")
        if(indexPath.row == 1)
        {
            print("Remove")
            
            //self.navigationController?.popToRootViewControllerAnimated(true)
            CompanyInfo.sharedUserDetail.company_id = ""
        }
        
    }
    
    
}
