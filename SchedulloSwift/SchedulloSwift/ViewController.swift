//
//  ViewController.swift
//  SchedulloSwift
//
//  Created by FiveExceptions1 on 06/02/17.
//  Copyright © 2017 FiveExceptions1. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    // username
    @IBOutlet var u_name: UITextField!
    //password
    @IBOutlet var password: UITextField!
    // picker view company
    var PickerViewCompany :UIPickerView!
    // picker view sorting
    var PickerViewSorting :UIPickerView!
    // picker view color
    var PickerViewColor :UIPickerView!
    
    // on logout stop side menu move
    var stopmenu : Bool = false
  
    
    //menu bar
    @IBOutlet var menuButton: UIBarButtonItem!
    
    var pickerDataSource1:[(name: String, val: String)] = []
    var pickedValue:Int = 0
        
    var btnClicked : Int = 0
    var companyListStore:[(name: String, val: String)] = []
    var sortingListStore:[(name: String, val: String)] = []
    var colorListStore  :[(name: String, val: String)] = []
    
    @IBOutlet var companyName_: UITextField!
    
    @IBOutlet var sortName_: UITextField!
    
    @IBOutlet var colorName_: UITextField!
    
    //LoginInfos
    var companyInfoList:[LoginInfo] = [LoginInfo]()
    
    
    
    let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    override func viewDidLoad() {
        super.viewDidLoad()
        u_name.text = "shubhamc322@gmail.com"
        
        password.text = "test12345"
        
        print("Number of company \(self.companyInfoList.count)-----\(CompanyInfo.sharedUserDetail.company_id )")
        
        if (CompanyInfo.sharedUserDetail.company_id != "") {
            companyListStore = []
            sortingListStore = []
            colorListStore = []
            
            self.stopmenu =  true
            
            self.companyName_.userInteractionEnabled = true
            self.sortName_.userInteractionEnabled = true
            self.colorName_.userInteractionEnabled = true
            
            loginUserWithJSONData {
                (json:JSON) in
                
                if let result = json["response"].string
                {
                    print("JSON viewDidLoad --\(result)")
                    print("JSON viewDidLoad --\(json)")
                    print("JSON color --\(self.colorListStore)")
                    print("JSON company --\(self.companyListStore)")
                    print("JSON sorting --\(self.sortingListStore)")
                    print("colro id \(TaskInfo.sharedUserDetail.color_id)")
                    
                    if result == "success" {
                        
                        let company_get_setting = json["company_list"].array
                        let color_get_setting = json["colors"].array
                        let sorting_get_setting = json["sorting"].array
                        
                        dispatch_async(dispatch_get_main_queue(),{
                            for reslt in company_get_setting! {
                                self.companyInfoList.append(LoginInfo(json: reslt))
                                self.companyListStore.append((reslt["company_name"].string!,reslt["company_id"].string!))
                                
                                if(reslt["company_id"].string! == CompanyInfo.sharedUserDetail.company_id)
                                {
                                    self.companyName_.text = reslt["company_name"].string
                                }

                            }
                            
                           
                            self.sortingListStore = [("Manual Sorting" , "1"), ("By Priority" , "2"), ("By Due Date", "3")]
                            
                            for i in 0...2{
                                
                                if(self.sortingListStore[i].val == TaskInfo.sharedUserDetail.priority_id){
                                    self.colorName_.text = self.sortingListStore[i].name
                                }
                            }
                            
                            for reslt in color_get_setting! {
                                self.colorListStore.append((reslt["color_name"].string!,reslt["user_color_id"].string!))
                                
                                    if(reslt["user_color_id"].string! == TaskInfo.sharedUserDetail.color_id)
                                    {
                                        self.sortName_.text = reslt["color_name"].string
                                    }
                                //
                            }
                        })
                    }// result json success
                    
                }// result
                
            }
        }
        else{
            self.stopmenu =  false
            self.navigationItem.leftBarButtonItem!.enabled = false;
            self.navigationItem.leftBarButtonItem!.tintColor = UIColor.whiteColor()
            
            // drop down disable
            self.companyName_.userInteractionEnabled = false
            self.sortName_.userInteractionEnabled = false
            self.colorName_.userInteractionEnabled = false
        }
        // Do any additional setup after loading the view, typically from a nib.
        
        
        if revealViewController() != nil && self.stopmenu{
            //            revealViewController().rearViewRevealWidth = 62
            menuButton.target = revealViewController()
            menuButton.action = "revealToggle:"
            
            //revealViewController().rightViewRevealWidth = 150
            //extraButton.target = revealViewController()
            //extraButton.action = "rightRevealToggle:"
            
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
        }
        else{
            print("Not moving")
        }
        
        
       
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func companySelectionList(sender: UITextField) {
        btnClicked = 0
        pickerDataSource1 = []
        print("company")
        pickerDataSource1 = self.companyListStore
        setupOfDropDown(sender)
    }
    
    @IBAction func sortingSelectionList(sender: UITextField) {
        btnClicked = 1
        pickerDataSource1 = []
        print("sorting")
        pickerDataSource1 = self.sortingListStore
        setupOfDropDown(sender)
    }
    
    @IBAction func colorSelectionList(sender: UITextField) {
        btnClicked = 2
        pickerDataSource1 = []
        print("color")
        //pickerDataSource1 = [("namo", "14"), ("namoha", "15"), ("ki ", "16"), ("jay", "17")]
        pickerDataSource1 = self.colorListStore
        setupOfDropDown(sender)
    }
    
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
//    {
//        print("/****/")
//        if (segue.identifier == "logout" ) {
//            
//            print("entered")
//            self.stopmenu =  false
//            //let navigationController = segue.destinationViewController as! UINavigationController
//           
//            //let questionViewController = navigationController.topViewController as! ViewController
//            
//            self.navigationItem.leftBarButtonItem!.enabled = false;
//            self.navigationItem.leftBarButtonItem!.tintColor = UIColor.whiteColor()
//            
//            CompanyInfo.sharedUserDetail.company_id = ""
//        }
//        
//    }
    
    @IBAction func onSaveClick(_ sender: UIButton) {
        print(Constant.BASE_URL)
        //var parrot = LoginInfo()
        // We write and read the results of the computed properties.
        //parrot.wingSpan = " hai "
        //print(parrot.wingSpan)
        if (CompanyInfo.sharedUserDetail.company_id == ""){
            loginUserWithJSONData {
                    (json:JSON) in
                print("JSON output --\(json) \(json["response"].string)")
                
                if let result = json["response"].string
                {
                    print("JSON inside --\(result)")
                    
                    if result == "success" {
                        self.stopmenu =  true
                        // bar button disable
                        self.navigationItem.leftBarButtonItem!.enabled = true;
                        self.navigationItem.leftBarButtonItem!.tintColor = UIColor.blueColor()
                        
                        self.companyName_.userInteractionEnabled = true
                        self.sortName_.userInteractionEnabled = true
                        self.colorName_.userInteractionEnabled = true
                        //LoginInfo.sharedUserDetail.email = self.u_name.text!
                        //LoginInfo.sharedUserDetail.password = self.password.text!
                                    
                        //LoginInfo.sharedUserDetail.companyInfo.append(json["company_list"].string)
                       
                      let allResults = json["company_list"].array
                      let allColors = json["colors"].array
                      let allSortings = json["sorting"].array
                    
                       for reslt in allResults! {
                            self.companyInfoList.append(LoginInfo(json: reslt))
                        self.companyListStore.append((reslt["company_name"].string!,reslt["company_id"].string!))
                       }
                        
                       self.sortingListStore = [("Manual Sorting" , "1"), ("By Priority" , "2"), ("By Due Date", "3")]
                        
                       for reslt in allColors! {
                           self.colorListStore.append((reslt["color_name"].string!,reslt["user_color_id"].string!))
                        
                       }
                        
                       dispatch_async(dispatch_get_main_queue(),{
                        if allResults!.count > 0 {
                            print("call one")
                            self.companyName_.becomeFirstResponder()
                        }
                        else{
                            let vc = self.storyBoard.instantiateViewControllerWithIdentifier("over") as! OverviewController
                            vc.myList = self.companyInfoList
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                       })
                    }
                    else{
                        self.navigationItem.leftBarButtonItem!.enabled = false;
                        self.navigationItem.leftBarButtonItem!.tintColor = UIColor.whiteColor()
                        
                        self.companyName_.userInteractionEnabled = false
                        self.sortName_.userInteractionEnabled = false
                        self.colorName_.userInteractionEnabled = false
                        print("Error while login.")
                    }
                }
                else{
                    print("Else part ")
                }
            }
        }
        else{
            let vc = self.storyBoard.instantiateViewControllerWithIdentifier("over") as! OverviewController
            
            vc.myList = self.companyInfoList
            
            self.navigationController?.pushViewController(vc, animated: true)

        }

    }
    
    func loginUserWithJSONData(onCompletion: (JSON) -> Void) {
        let plainString = password.text!
        let plainData = (plainString as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        let passInBase64 = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        print("\(passInBase64)------- ViewController")
       
        let param :[String:AnyObject] = ["email":u_name.text!,"password":passInBase64]
        
        print(param)
        
        //let newParam = NSJSONSerialization.
        
        //request.setValue("multipart/form-data; boundary", forHTTPHeaderField: "Content-Type")
        
        let url = "login"
        
        let route = Constant.BASE_URL+url
        
        let restAPIManager = RESTAPIManager()
        
        restAPIManager.makeHTTPPOSTRequest(route, body: param,onCompletion:
            {
                json, err in
                
                onCompletion(json as JSON)
                
        })
    }
    
    func setupOfDropDown(sender: UITextField){
        print("---Tag --- \(sender.tag)")
        
        if sender.tag == 15 {
            
            sortName_.resignFirstResponder()
            colorName_.resignFirstResponder()
        }
        else if sender.tag == 16 {
            companyName_.resignFirstResponder() // To resign the inputView for num of people on clicking done.
           
            colorName_.resignFirstResponder()
        }
        else if sender.tag == 17 {
            companyName_.resignFirstResponder() // To resign the inputView for num of people on clicking done.
            sortName_.resignFirstResponder()
        }
        
        
        
            print("UITapGestureRecognizer")
            //Create the view
            let inputView = UIView(frame: CGRectMake(0, 0, self.view.frame.width, 240))
            //self.view.addSubview(inputView)
            let inputButtonView = UIView(frame: CGRectMake(0, 0, self.view.frame.width, 40))
            inputButtonView.backgroundColor = UIColor.grayColor()
            inputView.addSubview(inputButtonView) // add UIView inside UIView
            
            //datepicker
            self.PickerViewCompany  = UIPickerView(frame: CGRectMake((self.view.frame.width/3) - 100, 40, 0, 0))
            self.PickerViewCompany.clipsToBounds = true
            self.PickerViewCompany.layer.borderWidth = 11
            self.PickerViewCompany.dataSource = self
            self.PickerViewCompany.delegate = self
            self.PickerViewCompany.showsSelectionIndicator = true
            inputView.addSubview(self.PickerViewCompany) // add date picker to UIView
            
            print("----\(self.view.frame.size.width)")
            
            // let image1 = UIImage(named: "foodicon.png")! as UIImage
            // let image2 = UIImage(named: "halal.png")! as UIImage
            
            // done button-----------
            let doneButton = UIButton(frame: CGRectMake(10, 0, 50, 40))
            
            
            doneButton.setTitle("Done", forState: UIControlState.Normal)
            doneButton.setTitle("Done", forState: UIControlState.Highlighted)
            doneButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
            doneButton.setTitleColor(UIColor.grayColor(), forState: UIControlState.Highlighted)
            doneButton.titleLabel?.font = UIFont.boldSystemFontOfSize(20)
            doneButton.tag = 12
            
            // doneButton.setBackgroundImage(image1, forState: UIControlState.Normal)
            
            inputView.addSubview(doneButton) // add Button to UIView
            
            doneButton.addTarget(self, action: "doneButton:", forControlEvents: UIControlEvents.TouchUpInside) // set button click event
            
            let titleLable = UILabel(frame: CGRectMake( (self.view.frame.width/2) - ((100/2)-20), 0,60, 40 ) )
            titleLable.text = "Date"
            titleLable.textColor = UIColor.whiteColor()
            titleLable.font = UIFont.boldSystemFontOfSize(20.0)
            titleLable.tag = 13
            
            inputView.addSubview(titleLable) // add Label to UIView
            
            // cancel-----------
            let cancelButton = UIButton(frame: CGRectMake(self.view.frame.width - 80, 0,70, 40))
            cancelButton.setTitle("Cancel", forState: UIControlState.Normal)
            cancelButton.setTitle("Cancel", forState: UIControlState.Highlighted)
            cancelButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
            cancelButton.setTitleColor(UIColor.grayColor(), forState: UIControlState.Highlighted)
            cancelButton.titleLabel?.font = UIFont.boldSystemFontOfSize(20)
            cancelButton.tag = 14
            //        cancelButton.setBackgroundImage(image2, forState: UIControlState.Normal)
            
            inputView.addSubview(cancelButton) // add Button to UIView
            
            cancelButton.addTarget(self, action: "cancelButton:", forControlEvents: UIControlEvents.TouchUpInside) // set button click event
            
            sender.inputView = inputView
            
            //  PickerView.addTarget(self, action: Selector("handleDatePicker:"), forControlEvents: UIControlEvents.ValueChanged)
            
            self.handleDatePicker(self.PickerViewCompany)
        
    }
    
    func handleDatePicker(sender: UIPickerView){
        print("handleDatePicker")
    }
    
    func numberOfComponentsInPickerView(PickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(PickerViewCompany: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        //pickedValue = 1
        return pickerDataSource1.count
    }
    
    func pickerView(PickerViewCompany: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickedValue = row
       
        print("didSelectRow --\(row)")
        //companyName_.text = pickerDataSource1[row].name
    }
    
    func pickerView(PickerViewCompany: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        print("titleForRow -- \(row)")
        //numOfPeople.text = pickerDataSource1[row]
        return pickerDataSource1[row].name
    }
    
    
    func doneButton(sender:UIButton)
    {
        companyName_.resignFirstResponder() // To resign the inputView for num of people on clicking done.
        sortName_.resignFirstResponder()
        colorName_.resignFirstResponder()
        
        print("done button -----\(pickedValue)")
        
        if sender.tag == 12 && btnClicked == 0
        {
            companyName_.text = pickerDataSource1[pickedValue].name
            
            print("----company---\(pickerDataSource1[pickedValue].val)")
            
            //self.companyListStore.count == 1
            
            if (CompanyInfo.sharedUserDetail.company_id != "") {
                //company selected name
                CompanyInfo.sharedUserDetail.company_id = pickerDataSource1[pickedValue].val
                let vc = self.storyBoard.instantiateViewControllerWithIdentifier("over") as! OverviewController
                
                vc.myList = self.companyInfoList
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else{
                //company selected name
                CompanyInfo.sharedUserDetail.company_id = pickerDataSource1[pickedValue].val
            }
        }
        else if sender.tag == 12 && btnClicked == 1
        {
            sortName_.text = pickerDataSource1[pickedValue].name
            
            TaskInfo.sharedUserDetail.priority_id = pickerDataSource1[pickedValue].val
            
            print("--------\(pickerDataSource1[pickedValue].val)")
        }
        else if sender.tag == 12 && btnClicked == 2
        {
            colorName_.text = pickerDataSource1[pickedValue].name
            
            TaskInfo.sharedUserDetail.color_id = pickerDataSource1[pickedValue].val
            print("--------\(pickerDataSource1[pickedValue].val)")
        }
    }
    
    func cancelButton(sender:UIButton)
    {
        if sender.tag == 14
        {
            companyName_.resignFirstResponder() // To resign the inputView for num of people on clicking done.
            sortName_.resignFirstResponder()
            colorName_.resignFirstResponder()
        }
    }
}// end of class

